%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        Corso Analisi Non Standard - Verona 2020
%
%                      Gruppo NSA-Verona
%
%                          Confronto
%
%                            testo
%
% This work may be distributed and/or modified under the
% conditions of the Creative Commons BY-SA
%
% The Current Maintainer of this work is 
% Daniele Zambelli - daniele.zambelli@gmail.com
%
% Copyright 2020-24 Daniele Zambelli
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Confronto}

\begin{frame}\frametitle{Metodi per il confronto}
L'insieme dei numeri Reali ha un ordinamento completo, se \(a\) e \(b\) sono 
due numeri reali qualunque è sempre valida una e una sola delle seguenti 
affermazioni:
\[a<b \quad a=b \quad b<a\]
Per confrontare due numeri Reali possiamo utilizzare le seguenti regole:

\begin{enumerate}
 \item qualunque numero negativo è minore di qualunque numero positivo;
 \item se due numeri sono negativi, è minore quello che ha il modulo 
maggiore;
 \item se \(a\) e \(b\) sono due numeri positivi, 
 \[a<b \sLRarrow a-b<0 \quad \text{ (o } \quad b-a>0 \text{ )}\]
oppure
 \[a<b \sLRarrow \frac{a}{b}<1 \quad \text{ (o } \quad \frac{b}{a}>1 
   \text{ )}\]
\end{enumerate}

\end{frame}

\begin{frame}\frametitle{Osservazioni sui metodi}
% \begin{osservazione}
Le prime due regole ci permettono di restringere le nostre riflessioni al 
solo caso del confronto tra numeri positivi.
Nei prossimi paragrafi assumeremo che le variabili si riferiscano solo a 
numeri positivi.
% \end{osservazione}

% \begin{osservazione}
Nella terza regola abbiamo presentato due criteri. Quello usato di solito
è il primo, ma useremo anche il secondo perché il rapporto tra due 
grandezze permette di ottenere informazioni interessanti.
% \end{osservazione}

\vspace{1em}

Anche negli Iperreali valgono le proprietà dei Reali richiamate sopra. 
Vediamo allora come è possibile affrontare il problema del confronto tra 
iperreali.

Restringendo l'osservazione ai numeri positivi possiamo affermare che gli 
infinitesimi sono più piccoli dei non infinitesimi e i finiti sono più 
piccoli degli infiniti:
\[i \quad < \quad fni \quad < \quad I\]
Passiamo ora al confronto all'interno dei diversi tipi di numeri iperreali.

\end{frame}

\begin{frame}\frametitle{Confronto tra finiti non infinitesimi}
Se due numeri iperreali hanno parte standard diversa allora è maggiore 
quello 
che ha la parte standard maggiore:
\[x < y \sLRarrow \st(x) < st(y)\]
Nel caso i due numeri abbiano la stessa parte standard si deve studiare 
l'ordinamento degli infinitesimi, cosa che faremo nel prossimo paragrafo.

\end{frame}

\begin{frame}\frametitle{}
Di seguito vediamo i diversi casi in cui ci possiamo 
imbattere quando vogliamo confrontare i numeri infinitesimi.

% \paragraph{Zero}
Zero è minore di qualunque infinitesimo positivo:
\[\epsilon-0 = \epsilon>0\]
% \paragraph{Somma}
La somma di infinitesimi positivi è maggiore di ognuno dei due:
\[\tonda{\epsilon+\delta}-\epsilon = \delta > 0\]
% \paragraph{Multiplo}
Il multiplo di un infinitesimo positivo è maggiore dell'infinitesimo di 
partenza. Usando il primo metodo per il confronto:
\[\forall n > 1 \quad n\epsilon-\epsilon = \tonda{n-1}\epsilon > 0\]
e usando il secondo metodo: 
\[\forall n > 1 \quad \frac{n\epsilon}{\epsilon} = n > 1\]
% \paragraph{Sottomultiplo}
Il sottomultiplo di un infinitesimo è minore dell'infinitesimo di partenza. 
Usando il primo metodo per il confronto:
\[\forall n > 1 \quad \frac{\epsilon}{n}-\epsilon = 
                      \frac{\epsilon-n\epsilon}{n} = 
                      \frac{\tonda{1-n}\epsilon}{n} < 0\]
e usando il secondo metodo: 
\[\forall n > 1 \quad \frac{\epsilon}{n}:\epsilon =
                      \frac{\epsilon}{n\epsilon} =
                      \frac{1}{n} < 1\]
% \begin{definizione}
 Diremo che \(\gamma\) e \(\epsilon\) sono \textbf{infinitesimi dello 
stesso ordine} se il rapporto tra \(\gamma\) e \(\epsilon\) è un 
finito non infinitesimo.
% \end{definizione}
\end{frame}

\begin{frame}\frametitle{Infinitesimo di infinitesimi}
% \paragraph{Parte infinitesima di un infinitesimo}
La parte infinitesima di un infinitesimo positivo è minore 
dell'infinitesimo di partenza. Se \(\gamma\) è un infinitesimo di 
un infinitesimo \(\epsilon\) cioè \(\gamma= \delta \epsilon\) allora:
\[\frac{\gamma}{\epsilon} = \frac{\delta \epsilon}{\epsilon}=\delta < 1\]
% \begin{osservazione}
In questo caso il rapporto non solo è più piccolo di~1 ma è addirittura un 
\emph{infinitesimo}, 
cioè~\(\gamma\) è una parte infinitesima di~\(\epsilon\). 
% Quando il rapporto tra due infinitesimi è un infinitesimo 
% cioè se~\(\gamma\) è un infinitesimo di \(\epsilon\)
In questo caso 
si dice che \(\gamma\) è un infinitesimo di \emph{ordine superiore} a 
\(\epsilon\) e si scrive:
\[\gamma=o(\epsilon)\]
% \end{osservazione}
% \begin{definizione}
 Diremo che \(\gamma\) è un \textbf{infinitesimo di ordine superiore} a 
\(\epsilon\) se il rapporto tra \(\gamma\) e \(\epsilon\) è un infinitesimo:
\[\gamma=o(\epsilon) \sLRarrow \frac{\gamma}{\epsilon}=\delta\]
Diremo anche che 
\(\epsilon\) è un \textbf{infinitesimo di ordine inferiore} a \(\gamma\).
% \end{definizione}

\end{frame}

\begin{frame}\frametitle{Confronto tra infiniti}
Anche tra gli infiniti possiamo effettuare il confronto calcolando la 
differenza tra due numeri o il quoziente e anche tra gli infiniti l'uso del 
quoziente ci dà delle informazioni interessanti.

% \paragraph{Infinito più finito}
Se \(a\) è un finito positivo (anche infinitesimo), confrontiamo \(M+a\) 
con \(M\). 
Usando il primo metodo:
\[M+a-M = a > 0\]
e usando il secondo metodo: 
\[\frac{M+a}{M} =
  \frac{M}{M} + \frac{a}{M} = 
  1 + \frac{a}{M} > 1\]
% \paragraph{Somma di infiniti}
Se \(M\) e \(N\) sono due infiniti positivi, confrontiamo \(M+N\) 
con \(M\). 
Usando il primo metodo:
\[M+N-M = N > 0\]
e usando il secondo metodo: 
\[\frac{M+N}{M} =
  \frac{M}{M} + \frac{N}{M} = 
  1 + \frac{N}{M} > 1\]
% \paragraph{Multiplo}
Se \(n>1\), confrontiamo \(nM\) con \(M\). 
Usando il primo metodo:
\[\forall n>1 \quad nM-M = \tonda{n-1}M > 0\]
e usando il secondo metodo: 
\[\frac{nM}{M} = n > 1\]
% \begin{definizione}
 Diremo che \(M\) e \(N\) sono \textbf{infiniti dello stesso ordine}  
se il rapporto tra \(M\) e \(N\) è un finito non infinitesimo.
% \end{definizione}
\end{frame}

\begin{frame}\frametitle{Infinitamente infinito}
% \paragraph{Infinito di infinito}
Confrontiamo \(MN\) con \(M\). 
Usando il primo metodo:
\[MN-M = \tonda{N-1}M > 0\]
e usando il secondo metodo: 
\[\frac{MN}{M} = N > 1\]
% \begin{osservazione}
In questo caso il rapporto non solo è maggiore di~1 ma è addirittura un 
\emph{infinito}. 
% \begin{definizione}
 Diremo che \(M\) è un \textbf{infinito di ordine superiore} a 
\(N\) se il rapporto tra \(M\) e \(N\) è un infinito:
\[M=\omega(N) \sLRarrow \frac{M}{N}=I\]
Diremo anche che 
\(N\) è un \textbf{infinito di ordine inferiore} a \(M\).
% \end{definizione}
% Quando il rapporto tra due infiniti è un infinito cioè 
% se~\(A\) vale infinite volte~\(B\) diremo che~\(A\) è un infinito di 
% \emph{ordine superiore} a~\(B\).
% \end{osservazione}
% 
% \vspace{1em}
% Ora confrontiamo \(M\) con \(MN\) usando il secondo metodo: 
% \[\frac{M}{MN} = \frac{1}{N} = \epsilon < 1\]
% 
% \begin{osservazione}
% In questo caso il rapporto non solo è minore di~1 ma è addirittura un 
% \emph{infinitesimo}. 
% Quando il rapporto tra due infiniti è un infinitesimo cioè 
% se~\(B\) vale infinite volte~\(A\) diremo che~\(A\) è un infinito di 
% \emph{ordine inferiore} a~\(B\).
% \end{osservazione}

\end{frame}

\begin{frame}\frametitle{Altri confronti}
\vspace{1em}
A volte il confronto tra due iperreali è meno immediato dei casi precedenti:
% \begin{esempio}
 Confrontare \(M\) e \(2^M\). 
 Dobbiamo calcolare: \(\frac{M}{2^M}\). 
Possiamo usare un duplice trucco: 
\begin{itemize} 
 \item invece di confrontare \(M\) e \(2^M\) confrontiamo \(M^2\) e \(2^M\);
 \item invece che confrontare direttamente i due valori richiesti, vediamo 
come si comportano, con numeri naturali piccoli, le due funzioni 
\(y_1=x^2\) e \(y_2=2^x\)
\end{itemize}
% invece che confrontare direttamente i due 
% valori 
% richiesti, vediamo come si comportano, con numeri naturali piccoli le due 
% funzioni  
% % \(y_1=\angolare{x^2}\) e \(y_2=\angolare{2^x}\):
% \(y_1=x^2\) e \(y_2=2^x\):
\begin{center}
\begin{tabular}{cccccccc}
\(x^2\) & 0 & 1 & 4 & 9 & 16 & 25 & 36\\
\(2^x\) & 1 & 2 & 4 & 8 & 16 & 32 & 64
\end{tabular}
\end{center}
Possiamo vedere che dal quinto elemento in poi la prima successione è sempre
minore della seconda ed essendo l'infinito più grande di cinque 
otteniamo che \(2^M > M^2\) quindi possiamo scrivere:
\[\frac{M}{2^M} < \frac{M}{M^2} = \frac{1}{M} < 1\]
Ma \(\frac{1}{M}\) è un infinitesimo quindi \(M\) è un infinito di ordine 
inferiore a \(2^M\).
% \end{esempio}

\end{frame}

\section{Relazione di indistinguibilità}

\begin{frame}\frametitle{Grandezze indistinguibili}
Quando risolviamo un problema pratico, a noi serve, alla fine dei calcoli, 
ottenere un numero razionale, con un certo numero di cifre significative.
È chiaro che se il risultato di un calcolo è~\(4,37+5\epsilon\) sostituire 
questo risultato con il più semplice~4,37 non ci fa perdere in precisione, 
in questo caso~\(5\epsilon\) può essere trascurato.
Ben diverso è se all'interno di un calcolo  
otteniamo:~\(\epsilon+5\epsilon\), in questo caso non posso 
trascurare~\(5\epsilon\) anche se è una quantità infinitesima. 

In certi casi posso avere due espressioni diverse che, in prima 
approssimazione, possono essere considerate equivalenti. Quando è così dirò 
che i due numeri iperreali sono \emph{indistinguibili}.
Due numeri sono indistinguibili quando la differenza tra i due è 
infinitesima rispetto a ciascuno dei due.

% \begin{definizione}
Due numeri si dicono \textbf{indistinguibili} (simbolo:~\(\sim\)) se il 
rapporto tra la loro differenza e ciascuno di essi è un infinitesimo:
\[x \sim y \sLRarrow 
\tonda{\frac{y-x}{x} = \epsilon \quad \wedge \quad \frac{y-x}{y} = \delta}
\]
% \end{definizione}

% \begin{osservazione}
 È importante osservare che per poter applicare la definizione entrambi i 
numeri che vogliamo confrontare devono essere diversi da \emph{zero}.
Cioè nessun numero diverso da zero può essere considerato indistinguibile 
da zero:~\(\nexists~x \neq 0 ~|~x \sim 0\)
% \end{osservazione}

\end{frame}

\begin{frame}\frametitle{Indistinguibilità e finiti non infinitesimi}
Se due numeri finiti non infinitesimi differiscono per un 
infinitesimo, sono indistinguibili.

% \begin{teorema}
Due numeri \(x\) e \(y\), finiti non infinitesimi, 
sono indistinguibili se e solo se sono infinitamente vicini:
\[x \approx y \sLRarrow x \sim y\]
% \end{teorema}

% \begin{proof}
Iniziamo dimostrando che se sono infinitamente vicini allora sono 
indistinguibili:
\begin{center}
Ipotesi: \(\tonda{x,~y:\ fni \sand y = x+\epsilon} \qquad \sLRarrow \qquad\) 
Tesi: \(x \sim y\)
\end{center}
Dimostrazione
\[\frac{y-x}{x}=\frac{\tonda{x+\epsilon}-x}{x} = 
\frac{\epsilon}{x}= \gamma \quad \wedge \quad 
\frac{y-x}{y}=\frac{\tonda{x+\epsilon}-x}{y} = 
\frac{\epsilon}{y}= \delta
\]
Il teorema inverso dirà:
\begin{center}
Ipotesi: \(\tonda{x,~y:\ fni \sand x \sim y} \qquad \sLRarrow \qquad\) 
Tesi: \(x \approx y\).
\end{center}
Dimostrazione
\[\frac{y-x}{x} = \epsilon
\sRarrow y-x=\epsilon x \sRarrow x=y+\epsilon x=y+\beta
\]
(Il caso $\dfrac{y-x}{y}$ a questo punto è banale.)
% \end{proof}

\end{frame}

\begin{frame}\frametitle{Indistinguibilità e infinitesimi}
Per quanto riguarda gli infinitesimi, non basta che siano infinitamente 
vicini, infatti tutti gli infinitesimi sono infinitamente vicini tra di loro.
Per essere indistinguibili serve una condizione più ristretta.

% \begin{teorema}
Due numeri \(\alpha\) e \(\beta\), infinitesimi, 
sono indistinguibili se e solo se la loro differenza è un infinitesimo di 
ordine superiore.
\[\beta-\alpha = o(\alpha) \sLRarrow \alpha \sim \beta\] 
% \end{teorema}

% \begin{osservazione}
 Se due infinitesimi differiscono per un infinitesimo di ordine superiore 
allora sono dello stesso ordine, quindi la differenza sarà di ordine 
superiore sia al primo sia al secondo infinitesimo.
% \end{osservazione}

% \begin{proof}
Iniziamo dimostrando che se differiscono per un infinitesimo di ordine 
superiore allora sono indistinguibili:
\begin{center}
Ipotesi: \(\tonda{\alpha,~\beta:\ inn \sand \beta = \alpha +o(\alpha)}
\qquad \sLRarrow\qquad\) 
Tesi: \(\alpha \sim \beta\)
\end{center}
Dimostrazione
\[\frac{\beta-\alpha}{\alpha}=
\frac{\tonda{\alpha +o(\alpha)}-\alpha}{\alpha} = 
\frac{o(\alpha)}{\alpha}= \gamma \quad \wedge \quad 
\frac{\beta-\alpha}{\beta}=
\frac{\tonda{\alpha +o(\alpha)}-\alpha}{\beta} = 
\frac{o(\beta)}{\beta}= \delta
\]
Il teorema inverso dirà:
\begin{center}
Ipotesi: \(\tonda{\alpha,~\beta:\ inn \sand \alpha \sim \beta}
\qquad \sLRarrow \qquad\) 
Tesi: \(\beta-\alpha = o(\alpha)\)
\end{center}
Dimostrazione
\[\frac{\beta - \alpha}{\alpha} = \epsilon \sRarrow 
\beta - \alpha =\epsilon \alpha \sRarrow 
\beta - \alpha = o(\alpha)
\]
% \end{proof}

\end{frame}

\begin{frame}\frametitle{Indistinguibilità e Infiniti}
La situazione si ribalta se i due numeri sono infiniti infatti, in 
questo caso, sono indistinguibili anche se differiscono di 
un valore finito o addirittura infinito. 

Si può dimostrare il seguente
% \begin{teorema}
Due numeri \(M\) e \(N\), infiniti, 
sono indistinguibili se e solo se la loro differenza è un finito o 
un infinito di ordine inferiore.
\[M-N = a \sRarrow M \sim N\] 
e vale anche:
\[\tonda{M-N = P \text{ con P infinito di ordine inferiore }} 
\sRarrow M \sim N\] 
% \end{teorema}
% \begin{proof}
Di seguito dimostriamo che se differiscono per un finito allora 
sono indistinguibili:
\begin{center}
Ipotesi: \(\tonda{M,~N:\ I \sand N = M+a}
\qquad \sLRarrow\qquad\) 
Tesi: \(M \sim N\)
\end{center}
Dimostrazione
\[\frac{M-N}{M}=
\frac{M-\tonda{M +a}}{M} = 
-\frac{a}{M}= \epsilon \quad \wedge \quad 
\frac{M-N}{N}=
\frac{M-\tonda{N +a}}{N} = 
-\frac{a}{N}= \delta
\]
In modo analogo si può procedere con la seconda parte del teorema.
% \end{proof}

\end{frame}

\begin{comment}

\section{Uno}

\begin{frame}\frametitle{}
\end{frame}

\end{comment}

\begin{comment}

\subsection{Indistinguibili}
\label{subsec:insnum_indistinguibili}

Di seguito esploriamo i tre casi possibili.

\subsubsection{}
\label{subsubsec:insnum_finitini}

\subsubsection{}
\label{subsubsec:insnum_infinitesimi}

\subsubsection{}
\label{subsubsec:insnum_infiniti}

\subsection{Principio di tranfer}
\label{subsec:insnum_nonarchimedei}

Abbiamo applicato agli iperreali le operazioni aritmetiche con grande 
naturalezza estendendo i metodi e i risultati che già conosciamo nei Reali. 
Ma è possibile fare ciò per qualunque funzione? 
Sì, è possibile assumere che per ogni funzione definita nei Reali esista 
una corrispondente funzione con dominio e codominio negli Iperreali che, 
ristretta ai Reali, coincida con la funzione reale.
In questo modo tutto quello che è possibile fare con i numeri Reali lo si 
può fare anche con gli Iperreali.

\begin{osservazione}
 Non vale il viceversa. Dato che gli Iperreali estendono i Reali, ci sono 
delle funzioni che, definite negli Iperreali, non hanno un valore 
corrispondente nei Reali. Ad esempio la funzione iperreale \emph{parte 
standard} non ha una funzione corrispondente nei numeri reali.

\begin{esempio}
 Consideriamo ad esempio la funzione: 
\(f: x \mapsto \frac{1}{x} \quad \forall x \in \R\), definita per \(x\ne 0\)

È facile costruire la funzione \(\effestar\) (\emph{effe star}) con dominio e 
codominio negli Iperreali:

\(\effestar: x \mapsto \frac{1}{x} \quad \forall x \in \IR\), definita per 
\(x\ne 0\).

Ogni volta che \(\effestar\) è applicata a numeri standard (fni), si comporta 
come
la funzione \(f\), applicata a \(x \in \R\); ma, in più, la 
funzione~\(\effestar\):
\begin{itemize} [noitemsep]
 \item 
è definita anche per valori infinitamente vicini a zero e 
in questo caso dà come risultato un valore infinito che non è un numero 
reale;
 \item 
è definita anche per valori infiniti e
in questo caso dà come risultato un valore infinitesimo che non è un numero 
reale. 
\end{itemize}
\end{esempio}
\end{osservazione}

\end{comment}



\section{Conclusioni}

\begin{comment}

In conclusione, possiamo confrontare fra di loro i numeri iperreali 
utilizzando la differenza o il quoziente tra i numeri. L'uso del quoziente 
ci permette di ricavare un'informazione interessante l'ordine di 
infinitesimo o di infinito.
\begin{itemize} [noitemsep]
 \item un infinitesimo di ordine superiore è un infinitesimo infinitamente 
 più piccolo;
 \item un infinito di ordine superiore è un infinito infinitamente più 
grande.
\end{itemize}

\end{comment}

\begin{frame}\frametitle{Riassunto}
\begin{enumerate} %[<+->]
\item 

\item 

\item 
\item 
\item 
\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Se tutto questo sembra promettente\dots}
Dobbiamo precisare come:
\begin{itemize}
\item ;
\item .
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Il tuo contributo}
Per approfondire, affronta alcune delle seguenti questioni.
\begin{enumerate}
\item .
\item .
\item 
\item 
\item \dots
\end{enumerate}
\end{frame}

      %--------------------------------------------------------------%

\appendix 
