%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        Corso Analisi Non Standard - Verona 2020
%
%                      Gruppo NSA-Verona
%
%                          Integrali
%
%                           grafici
%
% This work may be distributed and/or modified under the
% conditions of the Creative Commons BY-SA
%
% The Current Maintainer of this work is 
% Daniele Zambelli - daniele.zambelli@gmail.com
%
% Copyright 2020-24 Daniele Zambelli
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Le aree e le somme di Riemann finite}

\begin{frame}{Il problema delle aree}
Tra i problemi che motivarono lo sviluppo del calcolo infinitesimale,
oltre al problema della velocità istantanea e della pendenza,
c’era anche il problema dell’area limitata da una generica curva chiusa e non
da una spezzata.
\spause
Evidentemente \green{la nozione di area è additiva}, cioè accostando varie aree,
senza sovrapporle, l’area totale è la somma delle aree componenti.
\spause
Perciò, si può pensare che \green{approssimare parti di aree opportunamente 
disegnate piuttosto che l’area intera, riduce la difficoltà del problema}.
\end{frame}

\begin{frame}{Il trapezoide}
Nel caso dell’area del trapezoide sotto una curva individuata
da una funzione $f$ su un certo intervallo $[a,b]$, si può dividere detta
area in fette mediante rette verticali ad una distanza fissa \(d\) tra loro: 
queste rette avranno ascisse $a_i = a+i\cdot d$, con $i\leqslant n$ (ove $n$ \`e il 
massimo naturale tale che $a+n\cdot d < b$). Prendiamo 
anche la retta di ascissa $b$ per delimitare l'ultima fetta.
\spause

Ma anche per ciascuna di queste fette è problematico calcolare l’area,
perché non sono completamente limitate da una spezzata.
\spause

\`E per\`o facile calcolare l’area di rettangoli che le approssimino.
\end{frame}


\begin{frame}[shrink]
{Somme di Riemann finite}
Da qui l’idea di sostituire ciascuna di queste fette con un rettangolo di base 
uguale alla larghezza $d$ della fetta e di altezza $f(a_i)$, l’altezza della fetta
lungo il suo lato sinistro (ad esempio) di ascissa $a_i$.
\pause

Si ottiene così una unione di rettangoli disgiunti, detta \green{plurirettangolo},
la cui area (detta \green{somma di Riemann} relativa a quella funzione $f$
sull’intervallo $[a,b]$ e a quella distanza fissata $d$ tra le rette verticali) 
sarà uguale alla somma delle aree dei singoli rettangoli $f(a_i)\cdot d$.
\pause
L’area del plurirettangolo sarà cio\`e
$$\left(\sum_{i=0}^{n-1} f(a_i)\cdot d\right) + f(a_n)\cdot(b-n\cdot d),$$
La diversa forma dell'ultimo addendo è dovuta alla possibilità che $b-a$ non sia multiplo naturale di $d$.
\end{frame}

\begin{frame}[shrink]{Somme di Riemann}
\begin{columns} %[t]
 \begin{column}{.54\textwidth}
%  \vspace{-.2 cm}
%  \begin{center}
\scalebox{.8}{\sommeriemann}
%    \includegraphics[height=4cm]{Figura_1.jpg}
%   \end{center}  
\end{column}
\begin{column}{.44\textwidth}
In situazioni favorevoli, l’area del plurirettangolo sar\`a sempre più 
vicina all’area cercata al diminuire della larghezza delle fette, e quindi
aumentando il numero delle fette.
\end{column}
\end{columns}
In particolare, se la funzione $f$ è crescente e se la larghezza $d$ di 
ciascuna delle fette di un plurirettangolo viene divisa per un numero 
naturale $m$, 
\green{il plurirettangolo con fette di larghezza $d/m$ contiene il precedente 
ed è contenuto nell’area da valutare, e le è più vicino.}
\pause

Di qui l’idea di considerare suddivisioni dell’area da valutare in fette
sempre più sottili, eventualmente di larghezza infinitesima.
\end{frame}

\begin{frame}{Confronto tra diverse somme di Riemann (1)}
\begin{center} \scalebox{.7}{\duesuddivisionia} \end{center}
Per questa via però si incontrano subito delle difficoltà: due plurirettangoli, 
uno, $A$, costruito con rettangoli di larghezza $d$ e 
l’altro, $B$, con rettangoli di diversa larghezza $e$ (eventualmente minore di 
$d$), possono non essere uno contenuto nell’altro, e può diventare macchinoso
valutare la differenza di area tra i due.
\vspace{10mm}
\end{frame}

\begin{frame}{Confronto tra diverse somme di Riemann (2)}
\begin{center} \scalebox{.7}{\duesuddivisionia} \end{center}
Per affrontare questa difficoltà consideriamo
\begin{itemize}
 \item le ascisse $a_i = a+i\cdot d$, con \(i\leqslant n\), della ripartizione 
di 
$[a,b]$ in tratti di lunghezza $d$ (fuorché eventualmente l'ultimo),
 \item le ascisse $b_j = a+j\cdot e$, con $j\leqslant m$, della ripartizione di
$[a,b]$ in tratti di lunghezza $e$ (fuorché eventualmente l'ultimo),
\end{itemize}
dove $n$ è il massimo naturale tale che $a+n\cdot d < b$ e $m$ \`e definito 
analogamente per l'altra suddivisione.
\end{frame}


\begin{frame}{Confronto tra diverse somme di Riemann (3)}
\begin{center} \scalebox{.7}{\duesuddivisionib} \end{center}
%\vspace {-1cm}
Si ottengono così $n+m+2$ punti di ripartizione $c_k$ dell’intervallo $[a,b]$,
con $k\leqslant n+m+1$ e dove \(c_{n+m+2}=b\), con distanza tra un punto e il
successivo  minore o uguale al minimo tra $d$ ed $e$. 

Se $d$ ed $e$ sono commensurabili può succedere che 
alcuni punti della ripartizione che danno il plurirettangolo $A$ coincidano 
con punti della ripartizione che danno il plurirettangolo $B$, come in ogni 
caso succede per \(a_0\) e \(b_0\).
\vspace{5mm}
\end{frame}

\begin{frame}{Confronto tra diverse somme di Riemann (4)}
\begin{center} \scalebox{.7}{\duesuddivisionic} \end{center}
\small{Ciascuna delle fette determinate dai punti $c_k$ è parte sia di un rettangolo 
del plurirettangolo $A$ che di un rettangolo del plurirettangolo $B$.

Si può facilmente evidenziare la differenza tra le due parti, che può 
essere a favore di uno o dell’altro dei plurirettangoli. 
\pause

\green{Sommando tutte queste differenze in valore assoluto si maggiora la
differenza tra le aree dei due plurirettangoli}.}
% \vspace{10mm}
\end{frame}

\begin{frame}{Confronto tra diverse somme di Riemann (5)}
\begin{center} \scalebox{.7}{\duesuddivisionic} \end{center}
La differenza totale  è dunque maggiorata da 
\(\sum\limits_{k=1}^{n+m+1}(c_{k+1}-c_{k})\;|f(c_k)-f(\tilde c_k)|\) 
{\footnotesize(dove \(\tilde c_k\) denota l'ultimo punto, a sinistra di $c_k$, 
appartenente ``all'altra suddivisione'')}. 
\spause[.3]
\green{
Questa maggiorazione della differenza corrisponde all'area di una figura a 
scalini che ha per base l’intervallo $[a,b]$ e altezze varie nei vari tratti}.

\end{frame}

\section{Le somme iperfinite di funzioni continue}
\begin{frame}{Somme di Riemann iperfinite}
Fissata la funzione $f$ e l’intervallo $[a,b]$,
%entro cui valutare l’area sotto la curva, 
una somma di Riemann può essere vista come una funzione reale $F_R$ 
della larghezza $d$ dei rettangoli che costituiscono il plurirettangolo. \pause
Così l’area del plurirettangolo $A$ sarà $F_R(d)$ e quella del plurirettangolo $B$ 
sarà $F_R(e)$.
\spause

Questa funzione $F_R$ ha una sua estensione naturale iperreale, e la si può 
calcolare  anche quando il valore della larghezza delle fette è un numero 
infinitesimo positivo $dx$: in tal caso avremo un numero ipernaturale infinito 
$N$ di fette, il massimo tale che $a+N\cdot dx < b$ (come si è visto 
analizzando i punti di ripartizione di un intervallo). 
Parleremo allora di \green{somma di Riemann infinita}.
\spause

Se la funzione \(f\) nell'intervallo \([a, b]\) è limitata fra due valori \(m\)
e \(M\), di modo che \(m\leqslant f(x) \leqslant M\), è evidente che le somme
finite e infinite di \(f\) sono comprese fra \(m(b-a)\) e \(M(b-a)\).
\end{frame}


\begin{frame}{Somme di Riemann iperfinite di funzioni continue}
Con un po' di fortuna, se la funzione $f$ che delimita l'area \`e abbastanza 
regolare, può succedere che per ogni larghezza infinitesima positiva $dx$ delle 
fette, la parte standard della somma di Riemann $F_R(dx)$ sia sempre la stessa. 
\pause
In particolare,  \green{questo succede se la funzione $f$ è continua 
sull’intervallo $[a,b]$}.
\spause
 
In tal caso si può dire che \green{tutte le somme di Riemann infinite} corrispondenti a 
quelle larghezze infinitesime delle fette \green{colgono, a meno di un infinitesimo, 
l’area cercata} e che questa può essere ragionevolmente fatta coincidere con la 
parte standard comune a tutte quelle somme di Riemann infinite.
\spause

Questa parte standard  viene detta \green{integrale definito da $a$ a $b$ della 
funzione $f$ rispetto all’infinitesimo $dx$} e scriveremo $$\int_a^b 
f(x)\;dx=\hbox{st}(F_{R}(dx)).$$
\end{frame}

\section{L'integrale non dipende da \(dx\)}
\begin{frame}{L'integrale non dipende da  $dx$}
Si vuole ora giustificare l’indipendenza da $dx$ di tale parte standard.\\
\pause
Sia dunque $f$ una funzione continua sull’intervallo $[a,b]$. Si è già visto come 
maggiorare la differenza al variare della larghezza degli intervallini quando 
si considerano somme di Riemann finite. Passando alle somme di Riemann infinite, 
si possono utilizzare le stesse considerazioni estese agli iperreali via transfer. 
\pause

\vspace{3mm}
Possiamo in questo modo valutare le differenze tra le somme infinite calcolate
per due distinti infinitesimi $\gamma$ e $\delta$: per transfer una 
maggiorazione della differenza sarà 
\begin{align}
\sum_{k=1}^{N+M+1}(c_{k+1} - c_{k})\cdot |f(c_k)-f(\tilde c_k)|,
\end{align}
dove \(c_{N+M+2}=b\), $N$ è il massimo ipernaturale tale 
che $a+N\gamma < b$, $M$ è il massimo ipernaturale tale che $a+M\delta < b$ e 
dove $\tilde c_k$ è individuato come per le suddivisioni finite.
\end{frame}

\begin{frame}{L'integrale non dipende da  $dx$}
\begin{columns} %[t]
 \begin{column}{.44\textwidth}
%  \vspace{-.2 cm}
\begin{center} \scalebox{.7}{\diffsommeinfinite} \end{center}  
\end{column}
\begin{column}{.54\textwidth}
Poiché $c_k$ e $\tilde c_k$ sono infinitamente vicini (la distanza da 
$c_k$ a $c_{k+1}$ \`e minore sia di $\gamma$ che di $\delta$) e la 
funzione $f$ è continua, la differenza in modulo $|f(c_k)-f(\tilde c_k)|$ 
\`e infinitesima e pertanto minore di un qualsiasi numero reale positivo, 
diciamo $h$. 
\end{column}
\end{columns}
\spause
Pertanto
\[\sum_{k=1}^{N+M+1}(c_{k+1} - c_k)\cdot |f(c_k)-f(\tilde c_k)| <
  \sum_{i=1}^{N+M+1}(c_{k+1} - c_k)\cdot h= h\cdot (b-a).\]
\end{frame}

\begin{frame}{L'integrale non dipende da  $dx$}
\begin{columns} %[t]
 \begin{column}{.44\textwidth}
%  \vspace{-.2 cm}
\begin{center} \scalebox{.7}{\diffsommeinfinite} \end{center}  
\end{column}
\begin{column}{.54\textwidth}
Fissato un qualunque numero reale positivo $r$ e prendendo $h=r/(b-a)$, la 
differenza tra le due somme di Riemann considerate è minore di $r$, sicché 
essa è un infinitesimo e le parti standard (cio\`e gli integrali) sono le 
stesse.
\end{column}
\end{columns}
\vspace{25mm}
\end{frame}

\section{L'integrale e le proprietà elementari}
\begin{frame}[shrink]{Propriet\`a elementari dell'integrale}
\begin{enumerate}[<+->]
 \item L’indipendenza dell'integrale dalla larghezza infinitesima delle fette
 in cui viene suddivisa l’area, se la funzione $f$ è continua su $[a,b]$ (ipotesi
 che manterremo d’ora in avanti), equivale a dire che 
\green{\(\int_a^bf(x)\;dx = \int_a^bf(u)\;du\)}, se $dx$ e $du$ sono due arbitrari 
infinitesimi positivi.

\item \green{$\int_a^af(x)\;dx = 0$} poiché è $0$ la somma di Riemann sia 
finita che infinita sull’intervallo $[a,a]$ la cui parte standard si sta valutando.

\item Sempre ricorrendo alle somme di Riemann,
\green{$\int_a^b(f(x)+g(x))\;dx = \int_a^bf(x)\;dx + \int_a^b g(x)\;dx$}, 
sicché anche \green{$\int_a^b(-f(x))\;dx = -\int_a^bf(x)\;dx$}.

\item È anche immediato che 
\green{$\int_a^bf(x)\;dx +\int_b^cf(x)\;dx = \int_a^cf(x)\;dx$}: 
basta scegliere $dx = (b-a)/N$ con $N$ ipernaturale infinito 
perché allora le somme di Riemann da cui si ottengono il primo e il secondo 
integrale si giustappongono esattamente fornendo subito il terzo integrale.
\end{enumerate}
\end{frame}

\begin{frame}[shrink]{Propriet\`a elementari dell'integrale}
\begin{enumerate}[<+->]
\setcounter{enumi}{4}
 \item Nel caso in cui l’estremo inferiore di integrazione \(a\) è maggiore 
 dell’estremo superiore \(b\) ci muoveremo da \(a\) a \(b\) con passi negativi. 
 Accettiamo cioè che $dx$ possa essere un infinitesimo negativo. 
Con questa estensione diventa immediato che 
\green{$\int_a^bf(x)\;dx = -\int_b^af(x)\;dx$}.

\item \green{Proprietà rettangolare}:
\green{$m\cdot (b-a) < \int_a^bf(x)\;dx < M\cdot (b-a)$}, dove 
$m$ è il minimo della funzione $f$ su $[a,b]$ e $M$ ne è il massimo. $m$ e $M$
ci sono nell’intervallo per la continuità di \(f\) nell’intervallo.
Infatti l’area valutata dall’integrale è compresa tra quelle del rettangolo
con altezza il minimo della funzione nell’intervallo e quella con altezza il
massimo della funzione in quel intervallo. \\
% La stessa stima garantisce la finitezza delle somme di 
% Riemann infinite di una funzione limitata. 
% Così si ha anche \green{$m < \frac{1}{b-a}\int_a^bf(x)\;dx < M$}.
\end{enumerate}
\end{frame}

\begin{frame}{Calcolo di somme di Riemann e di integrali}
Si noti come sia difficile valutare le parti standard di somme di Riemann anche 
se finite, ci si riesce solo per particolari funzioni. Quello che si fa 
eventualmente è di calcolare una somma di Riemann per un valore reale 
abbastanza 
piccolo della larghezza delle fette in cui è divisa l’area, relativamente al 
problema da risolvere, e accettare il valore così calcolato come una ragionevole 
approssimazione del valore cercato.
\end{frame}

\section{Il teorema fondamentale}
\begin{frame}{Il teorema fondamentale del calcolo integrale}
 Per calcolare un integrale definito si ricorre ad un metodo del tutto diverso 
dal ricorso alla sua definizione. \\
\vspace{5mm}
Si considera la \green{funzione integrale}: la funzione che ad ogni valore di
un estremo di integrazione fa corrispondere il valore dell’integrale (se esiste)
fino a quell' estremo, cioè  la funzione \green{$$F(x) = \int_a^xf(t)\;dt.$$}\\
Si mostra che la derivata di questa funzione è la funzione integranda $f$.
\spause

Così il problema si trasforma nel problema, molto diverso, della ricerca 
delle primitive della funzione integranda, cioè le funzioni che hanno per derivata la 
funzione integranda $f$.
\end{frame}

\begin{frame}{Dimostriamo il teorema fondamentale del calcolo integrale}
\small{Per ottenere la derivata della funzione integrale $F(x)$, in base alla 
definizione di derivata si deve anzitutto \green{stimare il rapporto incrementale 
dell’estensione non standard di $F$}, che è
$$\frac{F(x+dx)-F(x)}{dx} = \frac{\int_a^{x+dx}f(t)\;dt - \int_a^x 
f(t)\;dt}{dx} 
= \frac{\int_x^{x+dx}f(t)\;dt}{dx}.$$
Per quanto visto e grazie al transfer, l’ultima espressione è \green{una quantità 
compresa tra il minimo e il massimo della funzione} nell’intervallo da 
$[x,\;x+dx]$. 

Quest'ultimo ha lunghezza infinitesima, sicché, \green{per la continuità della 
funzione}, i valori assunti nell’intervallo infinitesimo sono tutti 
infinitamente vicini tra loro e in particolare anche il massimo, il minimo e
$f(x)$. 
\spause[.3]

Così, \(\int_x^{x+dx}f(t)\;d(t) \approx f(x)\cdot dx\) e,
poiché il rapporto incrementale è infinitamente vicino a $f(x)$ per ogni 
incremento infinitesimo non nullo $dx$, avremo \green{$F'(x)=f(x)$}.}
\end{frame}

\section{Conclusioni}

\begin{frame}\frametitle{Riassunto}
\begin{enumerate} %[<+->]
\item 
Si può approssimare l'area sottesa ad una curva, grafico di una funzione, 
 con un plurirettangolo e applicando la proprietà della somma di aree.
L'espressione matematica di questo approccio è la somma di Riemann finita.
\item 
Con una suddivisione iperinfinita di strisce di larghezza infinitesima si hanno
tutte le possibili somme di Riemann infinite. Nel caso di \(f\) continua, esse
danno risultati infinitamente vicini fra loro. 
Per calcolarne l'area si estendono al caso infinito le proprietà della somma
finita e si applicano le conoscenze sul calcolo con gli iperreali.
\item 
Si dice integrale definito di una \(f\) continua su un intervallo \([a, b]\) la
parte standard comune alle somme di Riemann infinite. L'integrale quindi non 
dipende dalla particolare suddivisione infinita dell'intervallo di integrazione.
\end{enumerate}
\end{frame}

\begin{frame}{Riassunto (segue)}
\begin{enumerate}
\setcounter{enumi}{3}
\item 
Il calcolo degli integrali attraverso le somme è difficile. Si definisce allora
la funzione integrale di \(f\), cioè l'integrale di \(f\) sull'intervallo 
\([a, x]\). Si dimostra che \(f\) è la derivata di tale funzione integrale
(teorema fondamentale). 
\item 
Grazie al teorema fondamentale si ha un metodo per integrare una \(f\) continua: 
si cerca di ricostruire l'espressione di una funzione che, una volta derivata, 
esprime \(f\). Tale metodo, come sappiamo, ha i suoi pro e contro.
\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Se tutto questo sembra promettente\dots}
Siamo al termine del corso. Ti proponiamo di:
\begin{itemize}
\item valutare se iniziare a insegnare il calcolo infinitesimale non standard.
\item tenere i contatti con chi già lo insegna o è intenzionato a farlo.
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Il tuo contributo}
Per approfondire, affronta alcune delle seguenti questioni.
\begin{enumerate}
\item Confronta come si può trattare questo argomento in Analisi Non 
Standard con la trattazione presente sul tuo libro di testo.
\end{enumerate}
\end{frame}
