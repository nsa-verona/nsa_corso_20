%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        Corso Analisi Non Standard - Verona 2020
%
%                      Gruppo NSA-Verona
%
%                    Estensione e transfer
%
%                            testo
%
% This work may be distributed and/or modified under the
% conditions of the Creative Commons BY-SA
%
% The Current Maintainer of this work is 
% Daniele Zambelli - daniele.zambelli@gmail.com
%
% Copyright 2020-24 Daniele Zambelli
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Superstrutture}

\begin{frame}\frametitle{Superstrutture}
I \blue{sistemi numerici} sono importanti per 
apprezzare quantità sulle quali si vuole operare, ma in matematica si studiano 
anche gli oggetti costituiti dalle \blue{relazioni} e \blue{funzioni} tra i 
numeri, 
e anche relazioni e funzioni tra gli oggetti così costruiti, proseguendo 
indefinitamente in questo processo. 
\pause

Riferendoci ai reali si studia la superstruttura  \(V\) costruita 
sull'insieme numerico \(\R\) indicandola con \(V(\R)\).
\pause

Poiché una superstruttura è ottenuta iterando 
detto processo, essa sarà:
\blue{
\[\bigcup{V_n(\R) \sstext{con} n \in \N}\] 
}
\pause
dove:
\begin{enumerate}
 \item \(V_n(\R)\) indica quanto ottenuto all'\(n\)-esimo stadio;
 \item \(V_0(\R) = \R\);
 \item \(V_{n+1}(\R) = V_n(\R) \cup \mathcal{P}(V_n(\R))\);
 \item \(\mathcal{P}(A)\) indica l'insieme dei sottoinsiemi di \(A\).
\end{enumerate}

% dove \(V_n(\R)\) indica quanto ottenuto 
% all'\(n\)-esimo stadio, partendo da \(V_0(\R) = \R\) e passando da uno stadio 
% al successivo aggiungendogli tutte le relazioni e funzioni costruibili dallo 
% stadio attuale, 
% cioè  \(V_{n+1}(\R) = V_n(\R) \cup \mathcal{P}(V_n(\R))\), dove 
% \(\mathcal{P}(A)\) indica l'insieme 
% dei sottoinsiemi di \(A\).
\end{frame}

\section{Transfer}

\begin{frame}\frametitle{Superstrutture elementarmente equivalenti}
Estendendo il campo dei reali agli iperreali interessa capire cosa succede 
dell'estensione della superstruttura sui reali (\(V(\R)\)) nella 
superstruttura sugli iperreali (\(V(\IR)\)). 
\spause[.5]

Di fatto, c’è un’estensione elementare \(\starf V(\R)\) di \(V(\R)\) che è 
contenuta in \(V(\IR)\); questa estensione elementare \(\starf V(\R)\) é 
l’ambiente giusto per fare l’analisi non standard.
\spause[.5]

Dire che l'estensione è elementare vuol dire che tutte le affermazioni vere 
nella superstruttura \(V(\R)\) sono vere anche nella struttura 
\(\starf V(\R)\), 
cioè le due non si possono distinguere con il linguaggio di \(V(\R)\). 
\spause[.5]
Così per dimostrare qualcosa in \(\starf V(\R)\) basta dimostrarlo in 
\(V(\R)\), e si usa dire di averlo dimostrato per \blue{transfer}, 
cioè trasferendo il risultato da \(V(\R)\) a \(\starf V(\R)\). 
\pause

Si può utilizzare il transfer anche nell'altra direzione, purché si 
tratti di risultati espressi nel linguaggio di \(V(\R)\).
\end{frame}

\section{Estensione}

\begin{frame}\frametitle{Estensioni di operazioni}
Per uno studio liceale ci si può limitare ai primi passi della costruzione di 
una superstruttura, considerando solo come estendere le funzioni e le 
relazioni sui reali nel passare agli iperreali.
\spause

Già si sono considerate la relazione d'ordine \(<\) e le funzioni binarie 
addizione, \(+\), e moltiplicazione, \(\times\), e le loro estensioni agli 
iperreali \(\starf[.4] <\), \(\starf[.1] +\), \(\starf[.1] \times\). 
\end{frame}

\begin{frame}\frametitle{Estensioni di funzioni}
Ora vorremmo capire cosa succede di una qualsiasi funzione sui reali 
nell'estenderla agli iperreali in modo opportuno. 
\spause

Affinché una funzione \(\overline{f}\) su un insieme 
\(\overline{S}\) che contiene \(S\) sia 
un'\blue{estensione di una funzione} \(f\) su \(S\) dovrà succedere che per 
gli elementi di \(\overline{S}\) che sono anche elementi di \(S\) le 
due funzioni operino nello stesso modo.
\spause

In particolare, se le funzioni sono unarie e \(\sigma \in S\), dovrà essere 
\(\overline{f}(\sigma) = f(\sigma)\). 
\spause

Però ciò non dà alcuna indicazione su 
\(\overline{f}(\tau) \stext{ se } \tau \in (\overline{S} \setminus S)\): \\
\(\overline{f}(\tau)\) potrebbe essere 
non definita o assumere uno dei valori di \(\overline{S}\), 
che sono infiniti nel caso \(\IR\).
\end{frame}

\begin{frame}\frametitle{Estensioni naturali, principio di estensione}
D'altra parte, una funzione su \(\R\) vuole rappresentare uno specifico 
fenomeno e la sua estensione a \(\IR\) dovrebbe rappresentare lo stesso 
fenomeno osservato nei dettagli anche infinitesimali. 
\spause
Sicché è naturale supporre che ogni funzione \(f\) su \(\R\), tra le infinite 
possibili estensioni, abbia un'unica estensione naturale \(\starf f\) su 
\(\IR\). 
\spause
Questa assunzione va sotto il nome di \blue{principio di estensione}.
\spause
Di fatto abbiamo già seguito questa ipotesi nel passare 
\begin{center}
da \(+\) a \(\starf[.1] +\) \quad e \quad 
da \(\times\) a \(\starf[.1] \times\).
\end{center}
\end{frame}

\begin{frame}\frametitle{Principio di soluzione}
Tuttavia, si vorrebbe capire meglio chi è \(\starf f\) su \(\IR\), per una 
qualsiasi funzione \(f\) su \(\R\), sicché, seguendo il suggerimento di 
Leibniz, si richiederà che abbia tutte le stesse proprietà di \(f\), 
cioè che non sia distinguibile da \(f\) mediante il linguaggio dei reali.
\spause[.5]
Quest'assunzione va sotto il nome di \blue{principio di soluzione}. 
% Esso può essere formulato in vari modi dipendenti da come viene introdotto 
% il linguaggio dei reali.
\spause[.5]
Anche se sono molte le estensioni di \(f\) che hanno tutte le stesse 
proprietà di \(f\) esprimibili nel linguaggio dei reali, diventa indifferente 
quale di esse scegliere, sicché si fa riferimento a una particolare di esse  
come unica estensione naturale, e la si indica come \(\starf f\). 
\spause[.5]
Delusione per questa vaghezza? 
\pause
\green{\begin{center}Essa è normale in matematica.\end{center}}
\end{frame}

\begin{frame}\frametitle{Accettazione di elementi inconoscibili}
Insieme dei sottoinsiemi dei naturali, totalità dei numeri reali, insieme dei 
naturali, insiemi infiniti, \dots 
\spause[.3]
Ciascuna di queste nozioni ha molti modelli non distinguibili mediante il 
linguaggio, e introducono elementi che non potranno mai essere determinati (per 
motivi di cardinalità). 
\spause[.3]
Che dire dell'insieme delle funzioni sui reali? 
Ciascun individuo può avere una propria idea di queste nozioni senza che ciò 
sia di ostacolo al dialogo e all'elaborazione grazie alle proprietà 
fondamentali convenute che si accordano con ciascuna delle varie visioni 
individuali.
\spause[.3]
Questo fenomeno si verifica anche nel determinare i reali tra gli iperreali. 
Abbiamo introdotto i Reali come classi di equivalenza di iperreali 
infinitamente vicini tra loro o come rappresentanti privilegiati di queste 
classi. 
\pause

Chi deve essere il rappresentante? \pause \green{Uno qualsiasi}.
\end{frame}

\begin{frame}\frametitle{Contraddizione apparente}
Se gli iperreali soddisfano tutte le proprietà soddisfatte
dai reali, allora dovrebbero soddisfare anche la proprietà di Archimede:
\pause
\begin{center}
\blue{
Per ogni reale positivo c'è un naturale diverso da zero tale che \\
il suo reciproco è minore del reale.
}
\end{center}
\pause
In questo caso, però, gli iperreali non potrebbero avere infinitesimi 
positivi, contro quanto si sta assumendo.
\spause[.5]
No, non si è raggiunta alcuna contraddizione, perché si è detto che gli 
iperreali devono soddisfare tutte le proprietà soddisfatte dai reali, 
e non si è fatta alcuna assunzione sugli \blue{insiemi} di iperreali e sugli 
\blue{insiemi} di reali. 
\spause[.5]
La proprietà di Archimede e la completezza del sistema dei reali non sono 
proprietà dei reali ma degli insiemi di reali. 
Passando dai reali agli iperreali sarà opportuno precisare anche cosa succede 
degli insiemi di reali estendendoli a insiemi di iperreali.
\end{frame}

\begin{frame}\frametitle{Estensione naturale di insiemi}
Un qualsiasi insieme \(S\) di reali, \(S \subset \R\), è determinato dalla 
sua funzione caratteristica \(\mathcal{C}_S\), che è una funzione totale 
(=definita ovunque) con codominio 
\(\{0,~1\}\) tale che \(\mathcal{C}_S(a) = 0 \stext{ sse } a \in S\), 
mentre \(\mathcal{C}_S(a)=1\) 
altrimenti, sicché \(S = \{a:~~ \mathcal{C}_S(a)=0\}\).
\spause[.5]
Si è già convenuto che ogni funzione reale abbia una sua estensione naturale 
sugli iperreali, in particolare ciò vale anche per le funzioni 
caratteristiche che saranno totali sugli iperreali con codominio 
\(\{0,~1\}\).
\spause[.5]
Queste possono essere usate per determinare l'estensione naturale iperreale 
\(\starf S\) di un insieme di reali S: 
\[\starf S = \{a:~~ \starf \mathcal{C}_S(a)=0\}\]
\end{frame}

\begin{frame}\frametitle{Funzione caratteristica di \(\IR\)}
Prendiamo come esempio, l'insieme \(\R\) di tutti i reali:
è caratterizzato dalla proprietà sempre soddisfatta dai suoi elementi di 
essere uguali a sé stessi. 
\pause
\green{
\begin{center}
\(\mathcal{C}_\R(a) = 0 \stext{ sse } a \in \R \stext{ sse } a=a\).
\end{center}
}
\spause[.5]
Sicché:
\green{
\begin{center}
\(\R = \{a:~~ \mathcal{C}_\R(a) = 0\} \sstext{ovvero} \R = \{a:~~ a=a\}\). 
\end{center}
}
\spause
Così l'insieme \(\IR\) degli iperreali sarà caratterizzato dalla proprietà 
sempre soddisfatta dai suoi elementi di essere uguali a sé stessi. 
\pause
\orange{
\begin{center}
\(\starf \mathcal{C}_\R(c) = 0 \stext{ sse } c \in \IR \stext{ sse } c=c\).
\end{center}
}
\spause[.5]
Sicché:
\orange{
\begin{center}
\(\IR=\{c:~~ \starf \mathcal{C}_\R(c)=0\} \sstext{ovvero} \IR=\{c:~~ c=c\}\). 
\end{center}
}
\end{frame}

\section{Ipernaturali}

\begin{frame}\frametitle{Estensione naturale di N}
È interessante vedere chi è l'estensione naturale \(\IN\) di \(\N\), cioè dei 
numeri naturali. 

Gli elementi di \(\IN\) vengono detti ipernaturali.
Sappiamo che ogni naturale ha un successore immediato e che tra un naturale e 
il suo successore immediato non ci sono naturali. 

Detto altrimenti, per ogni \(n \in \N\), \(n+1 \in \N\) e tra 
\(n \stext{ e } n+1\) non ci sono naturali. 

Cioè:
\begin{itemize}
\item se \(\mathcal{C}_N(n)=0\) allora \(\mathcal{C}_N(n+1) = 0\),
\item se \(\mathcal{C}_N(n)=0 \stext{ e } n<k<n+1\) allora 
\(\mathcal{C}_N(k)=1\)
\end{itemize}

Così \(\IN = \{a: \starf \mathcal{C}_N(a)=0\}\), e:
\begin{itemize}
\item se \(a \in \IN\) allora \(a+1 \in \IN\), 
\item se \(a<k<a+1\) allora \(\starf \mathcal{C}_N(k)=1\). 
\end{itemize}

Inoltre, se 
\(n \in \N \) (cioè \(\mathcal{C}_N(n)=0\)) allora 
\(\starf \mathcal{C}_N(n)=0\), \\
poiché \(\starf \mathcal{C}_N\) estende 
\(\mathcal{C}_N \stext{ e } n \in \IN\).
\end{frame}

% \begin{frame}\frametitle{Estensione naturale di N}
% È interessante vedere chi è l'estensione naturale \(\IN\) di \(\N\), cioè dei 
% numeri naturali. 
% 
% Gli elementi di \(\IN\) vengono detti ipernaturali.
% Sappiamo che ogni naturale ha un successore immediato e che tra un naturale e 
% il suo successore immediato non ci sono naturali. 
% 
% Detto altrimenti, per ogni \(n \in \N\), \(n+1 \in \N\) e tra 
% \(n \stext{ e } n+1\) non ci sono naturali. 
% 
% Cioè:
% se \(f_N(n)=0\) allora \(f_N(n+1) = 0\), e se 
% \(f_N(n)=0 \stext{ e } n<k<n+1 \stext{ allora } f_N(k)=1\).
% 
% Così, 
% \(\IN = \{a: \starf f_N(a)=0\}\), e se \(a \in \IN \stext{ allora } 
% a+1 \in \IN \stext{ e se } a<k<a+1 \stext{ allora } \starf f_N(k)=1\). 
% 
% Inoltre, se 
% \(n \in \N \) (cioè \(f_N(n)=0\)) allora \(\starf f_N(n)=0\), 
% poiché \(\starf f_N\) estende \(f_N \stext{ e } n \in \IN\).
% \end{frame}

\begin{frame}\frametitle{Per asteriscos ad astra}
Facendo corrispondere a un insieme di reali la sua estensione naturale tra 
gli iperreali, si può richiedere che si mantengano anche le proprietà che 
coinvolgono insiemi, mediante le funzioni caratteristiche.
\spause[.5]
Così, nell'esprimere mediante il linguaggio le proprietà che si devono 
mantenere si possono considerare anche proprietà che includano i nomi di 
particolari insiemi purché, nell'interpretare le espressioni nella struttura 
estesa, si faccia riferimento alle estensioni naturali dei particolari insiemi.
\spause[.5]
Tipograficamente ciò si ottiene trasformando le espressioni da mantenere 
soddisfatte anteponendo un asterisco ai nomi di insiemi.
\end{frame}

% \begin{frame}\frametitle{Proprietà iper-archimedea}
% Ad esempio, la proprietà di Archimede che, come si è visto non si mantiene 
% nell'estensione agli iperreali perché parla di particolari insiemi 
% di reali, diventa una proprietà degli elementi, e si deve mantenere, se si 
% considerano le estensioni naturali degli insiemi.
% % \spause[.5]
% % Così la proprietà che per ogni reale positivo c'è un naturale positivo il 
% % cui reciproco è minore del reale dato, diventa la proprietà 
% % (iper-archimedeità) 
% % che per ogni iperreale positivo c'è un ipernaturale positivo il cui 
% % reciproco è minore dell'iperreale, proprietà che deve essere vera tra gli 
% % iperreali come applicazione del principio di Leibniz.
% \spause[.5]
% Così la proprietà archimedea, diventa \\
% la \orange{proprietà iper-archimedea}:
% \pause
% \begin{center}
% \blue{
% Per ogni iperreale positivo c'è un ipernaturale positivo \\
% il cui reciproco è minore dell'iperreale.
% }
% \end{center}
% \pause
% Proprietà che deve essere vera tra gli iperreali come applicazione del 
% principio di Leibniz.
% \spause[.5]
% In particolare, se l'iperreale è un infinitesimo positivo \(\epsilon\), ci 
% dovrà essere un ipernaturale \(n\) il cui reciproco \(\frac{1}{n}\) è un
% infinitesimo positivo, dovendo essere minore di \(\epsilon\). 
% 
% Tale ipernaturale dovrà essere un infinito positivo, maggiore di ogni 
% naturale standard. 
% \end{frame}

\begin{frame}\frametitle{Proprietà iper-archimedea}
La proprietà di Archimede che, come si è visto non si mantiene 
nell'estensione agli iperreali perché parla di particolari insiemi di reali, 
diventa una proprietà degli elementi, e si deve mantenere, se si considerano 
le estensioni naturali degli insiemi.
% \spause[.5]
% Così la proprietà che per ogni reale positivo c'è un naturale positivo il 
% cui reciproco è minore del reale dato, diventa la proprietà 
% (iper-archimedeità) 
% che per ogni iperreale positivo c'è un ipernaturale positivo il cui 
% reciproco è minore dell'iperreale, proprietà che deve essere vera tra gli 
% iperreali come applicazione del principio di Leibniz.
\pause
\vspace{-3mm}
\begin{columns} % [T] % align columns
\begin{column}{.50\textwidth}
\begin{center}
\orange{p. archimedea}\\
\blue{Per ogni reale positivo c'è un naturale positivo \\
il cui reciproco è minore \\ del reale.}
\end{center}
\end{column}%
% \hfill%
\begin{column}{.50\textwidth}
% \vspace*{.5mm}
\begin{center}
\orange{p. iper-archimedea}\\
\blue{Per ogni iperreale positivo c'è un ipernaturale positivo \\
il cui reciproco è minore dell'iperreale.}
\end{center}
\end{column}%
\end{columns}
\spause[.5]
Proprietà che deve essere vera tra gli iperreali come applicazione del 
principio di Leibniz.
\spause[.5]
In particolare, se l'iperreale è un infinitesimo positivo \(\epsilon\), ci 
dovrà essere un ipernaturale \(n\) il cui reciproco \(\frac{1}{n}\) è un
infinitesimo positivo, dovendo essere minore di \(\epsilon\). 
\pause
\begin{center}
\green{Tale ipernaturale dovrà essere un infinito positivo.}
\end{center}
\end{frame}

\begin{frame}\frametitle{Alcune proprietà degli ipernaturali}
\begin{enumerate}[<+->]
\item 
\green{\(\IN \setminus \N\) non è vuoto}, cioè ci sono ipernaturali che non 
sono naturali. 
Essi dovranno seguire i naturali usuali e saranno opportunamente chiamati 
(iper)naturali infiniti.
\item 
Poiché per ogni reale positivo \(r\) c'è un massimo naturale \(n\) che gli è 
minore od uguale (parte intera di \(r\)), cioè tale \green{\(n \leqslant r < n+1\)}, 
così anche per ogni 
iperreale positivo \(\rho\) ci sarà un massimo ipernaturale \(\nu\) che gli è 
minore o uguale, cioè tale che \green{\(\nu \leqslant \rho < \nu+1\)}.
\item 
Poiché in ogni \green{insieme finito} di reali gli elementi si possono 
confrontare a due a due per decidere chi è il massimo o chi è il minimo o chi 
possiede qualche altra caratteristica, 
questa proprietà deve valere anche per gli 
\green{insiemi iperfiniti} (cioè in biiettività con un ipernaturale) di 
iperreali.
\end{enumerate}
\end{frame}

\section{Altri insiemi iperfiniti}

\begin{frame}\frametitle{Iperinteri e iperrazionali}
Tra gli iperreali, ciascun ipernaturale avrà il suo opposto e gli 
ipernaturali con i loro opposti costituiscono l'insieme degli iperinteri. 
\spause[.5]
Analogamente, ogni iperintero diverso da zero ha un reciproco tra gli 
iperreali, e si possono considerare i rapporti tra iperinteri con 
denominatore diverso da zero.
\spause[.5]
Ovviamente, per transfer: 

due rapporti\quad  \(a~ \starf /~ b \texte c~ \starf /~ d\) \quad
di iperinteri\quad  \(a,~b,~c,~d\) 
indicano lo stesso iperreale sse \quad 
\blue{\(a~ \starf[.3] \times d = b~ \starf[.3] \times c\)}. 
\spause[.5]
Così si stabilisce una relazione di equivalenza tra rapporti di iperinteri, 
ottenendo gli iperrazionali come classi di equivalenza rispetto a tale 
relazione (o come rappresentanti di queste classi).
\spause[.5]
Sempre per transfer, tra due diversi iperreali c'è sempre un iperrazionale 
che li separa anche se i due sono infinitamente vicini.
\end{frame}

\section{Osservazioni finali}

\begin{frame}\frametitle{Problema di notazione}
Per sottolineare la differenza tra una funzione reale e la sua estensione 
naturale, finora si è prestata molta attenzione nel denotare le estensioni 
naturali di funzioni reali facendo precedere un asterisco al simbolo della 
funzione reale che veniva estesa.
\spause[.5]
Ciò provoca una scrittura piena di asterischi che può rendere pesante la 
lettura.
\spause[.5]
D'altra parte, si possono evitare tutti gli asterischi senza perdere 
informazioni su quello che si sta facendo. 
\spause[.5]
Basta convenire che se tra gli argomenti cui si applica la funzione compaiono 
iperreali allora il simbolo di funzione, che è quello di una funzione reale, 
denota l'estensione naturale della funzione reale indicata.
\spause[.5]
Nel seguito questa convenzione sarà adottata regolarmente.
\end{frame}

\begin{frame}\frametitle{st non è estensione naturale}
Come si è visto, ci interessano le funzioni sugli iperreali che sono 
estensioni naturali di funzioni sui reali. 
Ma queste non sono le sole funzioni sugli iperreali che si usano per la loro 
rilevanza.
\spause[.5]
Già abbiamo considerato la funzione parte standard. Essa è una funzione sugli 
iperreali non totale né suriettiva né iniettiva che a ogni iperreale finito 
associa il reale che è la sua parte standard. 
\spause[.5]
Questa funzione non è estensione naturale di nessuna funzione sui reali. 

Infatti, la sua restrizione ai reali è la funzione identica sui reali, sicché 
se fosse l'estensione naturale di una funzione reale dovrebbe essere 
l'estensione naturale della funzione identica, ma questa estensione è la 
funzione identica sugli iperreali che è ben diversa dalla funzione parte 
standard. 
\end{frame}

\begin{comment}

\begin{frame}\frametitle{}
\end{frame}

\begin{frame}\frametitle{}
% {\fontsize{8pt}{7.2}\selectfont}
\end{frame}

\begin{frame}[shrink=5]
\frametitle{}
\end{frame}

\end{comment}

\section{Conclusioni}

\begin{frame}\frametitle{Riassunto}
\begin{enumerate} %[<+->]
\item 
La matematica non si interessa solo di numeri, ma anche delle strutture 
costruite sui numeri e delle strutture costruite sulle strutture.
\item 
Tra superstrutture elementarmente equivalenti vale il principio di transfer.
\item 
Estensioni naturali e principi di estensione e di soluzione.
\item 
Possiamo anche considerare gli ipernaturali, gli iperinteri e gli 
iperrazionali.
\item 
Come per altro siamo stati abituati a fare nell'estensione dei 
vari insiemi numerici, anche nel passaggio dai reali agli 
iperreali semplifichiamo la notazione dando lo stesso nome a funzioni 
diverse, ma corrispondenti.
\item
La parte standard non è estensione naturale di una funzione reale.
\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Se tutto questo sembra promettente\dots}
Possiamo:
\begin{itemize}
\item Affrontare la continuità.
\item Ridefinire i limiti.
\item Affrontare i teoremi sulle funzioni continue. 
\item Affrontare i teoremi sulle funzioni derivabili.
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Il tuo contributo}
Per approfondire, affronta alcune delle seguenti questioni.
\begin{enumerate}
\item Nel tuo testo di matematica, come è gestita l'estensione dai naturali 
agli interi e agli altri insiemi numerici?
% \item 
% \item 
% \item 
% \item 
% \item 
\item \dots
\end{enumerate}
\end{frame}

      %--------------------------------------------------------------%

