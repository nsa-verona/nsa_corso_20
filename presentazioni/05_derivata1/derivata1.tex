%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        Corso Analisi Non Standard - Verona 2020
%
%                      Gruppo NSA-Verona
%
%                         Derivata 1
%
%                           testo
%
% This work may be distributed and/or modified under the
% conditions of the Creative Commons BY-SA
%
% The Current Maintainer of this work is 
% Daniele Zambelli - daniele.zambelli@gmail.com
%
% Copyright 2020-24 Daniele Zambelli
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Quello che abbiamo già visto}


\begin{frame}\frametitle{Quello che abbiamo già visto}
\fontsize{15pt}{7.2}\selectfont
\begin{itemize}%[<+->]

 \item <1-> Pendenza \(\longrightarrow\) Numeri Trascurabili\\
 \vspace{2mm}
{\fontsize{12pt}{7.2}\selectfont 
(Da esperienze di pendenza puntuale  alla necessità di numeri trascurabili)}
 \vspace{5mm}
 \item <2-> \(\IR\)\vspace{5mm}
 \item <3-> \(\st\)\vspace{5mm}
 \end{itemize}
\only <4-> {
Ora possiamo esprimere esplicitamente cosa intendiamo per pendenza in un punto.}
\end{frame}

\section{Definizione precisa della pendenza}

\begin{frame} [shrink=10] 
\frametitle{La pendenza in un punto} %Il problema della pendenza}
\begin{columns} [t]
\begin{column}{.45\textwidth}
 La pendenza vista al microscopio
\pendenza
\end{column}
 \begin{column}{.52\textwidth}
 {\fontsize{12pt}{7.2}\selectfont
 Siano:
 \begin{itemize}% [<+->]
  \item <1-> \(x_0\) l'ascissa di un punto in cui vogliamo calcolare la 
  pendenza di una funzione \(f\);
  \item <2-> \(\Delta x\) un infinitesimo non nullo, usato come incremento 
della 
  variabile indipendente;
  \item <3->\(f(x_0+\Delta x) -f(x_0)\) l'incremento conseguente per la funzione
  \(f\), anche denotato con \(\Delta y\).
  \end{itemize}
  \only <4-> {Allora \(\Delta y\) dipende in generale da tre variabili: 
  \(\Delta y(f, x_0,\Delta x)\).\vspace{.3cm}}
    \only <5-> {
  Fissando \(f\) e \(x_0\): \(\Delta y\) dipende solo da \(\Delta x\).\\
  \vspace{.3cm}}
  \only <6-> {
  Fissando solo \(f\):\\\(\Delta y=\Delta y(x_0,\Delta x)=f(x_0+\Delta 
   x)-f(x_0)\)} 
   }
 \end{column}
\end{columns}
\end{frame}


\begin{frame}\frametitle{La pendenza in un punto}
% Avendo esplicitato i dati della situazione, definiamo.\\
% \vspace{3mm}
Si dice {\color{green!50!black}pendenza} della funzione \(f\) nel suo punto di
ascissa \(x_0\) {\color{green!50!black}la parte standard delle pendenze
di tutte le secanti per tale punto di ascissa \(x_0\) e per un altro suo punto
di ascissa \(x_0+\Delta x\) infinitamente vicina alla precedente}, 
{\color{red!50!black}se tutte queste pendenze 
\(\frac{f(x_0+\Delta x) - f(x_0)}{\Delta x}\) sono sostanzialmente la stessa},
cioè se hanno tutte parte standard che è sempre la stessa.
\spause


La pendenza non è la parte standard 
del rapporto incrementale per un qualche incremento infinitesimo non nullo 
\(\Delta x\), ma lo è se (e ciò è essenziale) le parti standard di tutte 
queste secanti sono tra loro uguali per ogni infinitesimo \(\Delta x\) 
diverso da \(0\).
 
\end{frame}

\section{Esempi} %e controesempi}
\begin{frame}\frametitle{Un esempio}
\fontsize{10pt}{7.2}\selectfont{
Calcolo della pendenza della funzione  \quad \(f(x)=-x^2+2x+1\) \\ 
nel suo punto di ascissa \(\frac{3}{2}\).}

Per \(\Delta x\) infinitesimo non nullo, si calcoli la parte standard del
rapporto incrementale:
\fontsize{8pt}{7.2}\selectfont{
\noindent\begin{align*}
\pst{\frac{f(\frac{3}{2}+\Delta x) - f(\frac{3}{2})}{(\frac{3}{2} + \Delta x)
   - \frac{3}{2}}} 
&=\pst{\frac{-\tonda{\frac{3}{2}+\Delta x}^2+2 \tonda{\frac{3}{2}+\Delta x} +1
-\tonda{-\tonda{\frac{3}{2}}^2+2\cdot\frac{3}{2}+1}}{\Delta x}} =\\
&=\pst{\frac{\cancel{-\frac{9}{4}}-3\Delta x-\tonda{\Delta x}^2~\cancel{+3} 
+2\Delta x~\cancel{+1}~ \cancel{+\frac{9}{4}}~\cancel{-3}~\cancel{-1}}
             {\Delta x}} =\\
&=\pst{\dfrac{\cancel{\Delta x}\tonda{-3+2-\Delta x}}{\cancel{\Delta x}}}\quad
\overset{(*)}{=}\quad\pst{-1}+\pst{-\Delta x} 
\overset{(**)}{=}-1.
\end{align*}
}
\((*)\) poiché \(\Delta x \ne 0\) e per le proprietà della funzione \(\st\).\\
\((**)\) poiché la parte standard di un reale è il reale stesso e la parte 
standard di un infinitesimo è zero.
\spause
\fontsize{10pt}{7.2}\selectfont{
Non dipendendo da \(\Delta x\), questa parte standard è sempre la stessa 
per qualsiasi \(\Delta x\) infinitesimo non nullo. Ne consegue che \quad \(-1\)
\quad è la pendenza di \(f\) nel punto considerato: \(f'(\frac{3}{2})=-1\).}
\end{frame}

\begin{frame}\frametitle{Un dubbio possibile}
L'indipendenza della parte standard dai valori di \(\Delta x\) 
è stata giustificata nel caso della scelta particolare \(x=\frac{3}{2}\).\\

Tuttavia, per questa funzione, l'indipendenza da \(\Delta x\) vale anche per
ogni altro valore dato alla variabile indipendente. \\
Infatti sia \(f(x)=-x^2+2x+1\) e \(x=a\).\\
%\vspace{3mm}

\fontsize{8pt}{7.2}\selectfont{
\noindent\begin{align*}
\pst{\dfrac{f(a+\Delta x) - f(a)}{(a + \Delta x) - a}} &=
\pst{\dfrac{-\tonda{a+\Delta x}^2+2 \tonda{a+\Delta x} +1 
-\tonda{-a^2+2a+1}}{\Delta x}} =\\
&=\pst{\dfrac{\cancel{-a^2}-2a\Delta x-\tonda{\Delta x}^2~\cancel{+2a} +2\Delta 
x~\cancel{+1}~\cancel{+a^2}~\cancel{-2a}~\cancel{-1}}{\Delta x}}=\\ 
&=\pst{\dfrac{\cancel{\Delta x}\tonda{-2a+2-\Delta x}}
         {\cancel{\Delta x}}}
  \overset{*}{=}
  \pst{-2a+2+\Delta x}
  \overset{**}=-2a+2.
\end{align*}
}
\fontsize{8pt}{7.2}\selectfont{
(*) Indipendentemente dal valore di \(\Delta x\ne 0\). \\
(**) Per le proprietà della funzione parte standard.\\
}
% \vspace{3mm}
\fontsize{10pt}{7.2}\selectfont{
Non dipendendo da \(\Delta x\), questa parte standard è sempre la stessa 
per qualsiasi \(\Delta x\) infinitesimo non nullo.
La pendenza \(f'(a)\) è: \(-2a+2\).}
\end{frame}


%\section{Dalla pendenza alla derivata}

\begin{frame}\frametitle{Pendenze  per alcuni valori di \(a\)}
\(f'(a)\) indica quindi le pendenze della curva \(f\), se esistono. Esse 
cambiano valore al variare di \(a\). Per esempio:
{\footnotesize
\begin{center}
\begin{tabular}{c c c c c c c c c}
\(a\)     & \(-2\)  & \(-1\)  & \(0\)  & \(+\frac{1}{2}\)&\(+1\)   & \(+\frac{3}{2}\) 
 & \(+2\) & \(+3\)\\
\(f'(a)\) & \(+6\)  & \(+4\)  & \(+2\) & \(+1\)           & \(~~~0\)&  \(-1\)
 & \(-2\) &  \(-4\)\\ 
\end{tabular}
\end{center}
}
\begin{columns} [T]
\begin{column}{.48\textwidth}
\pendenzaparabolaA
\end{column}%
% \hfill%
\begin{column}{.48\textwidth}
% \vspace{-2cm}
\pendenzaparabolaB
\end{column}%
\end{columns}
\end{frame}

\section{Controesempi}
\begin{frame}\frametitle{Se il rapporto incrementale non ha parte standard}
Si cerchi di calcolare la pendenza \(f'(0)\) per la funzione 
\(f(x)=\sqrt[3]{x^2}\). Si noti che \(f(0)\) c’è e vale \(0\).\\
La parte standard del rapporto incrementale è:
\spause[-1]
\[
 \pst{\frac{\sqrt[3]{(0+\Delta x)^2}-\sqrt[3]{0^2}}{\Delta x}}=
 \pst{\frac{\sqrt[3]{(\Delta x)^2}}{\Delta x}}=\pst{\frac{1}{\sqrt[3]{\Delta x}}}.
\]
\pause
\(f'(0)\) non c'è. Infatti,\\

qualsiasi sia l'infinitesimo non nullo \(\Delta x\), 
\(\frac{1}{\sqrt[3]{\Delta x}}\) è un iperreale infinito e non esiste la parte
standard  di un numero infinito.\\
\vspace{3mm}
Ciò è sufficiente per concludere che \(f\) non ha pendenza in \(0\).
\end{frame}

\begin{frame}\frametitle{Se la parte standard del rapporto incrementale 
dipende da \(\Delta x\)}
Si cerchi di calcolare \(f'(0)\) per la funzione \(f(x)=\abs{x}\).\\
\spause
Per \(\Delta x\) infinitesimo non nullo, si ha:
\[
 \pst{\frac{\abs{0+\Delta x}-\abs{0}}{\Delta x}}=
 \pst{\frac{\abs{\Delta x}}{\Delta x}}=
  \begin{cases}
   1  & \text{se  $\Delta x >0,$}\\
   -1 & \text{se  $\Delta x <0$.}
 \end{cases}
\]
\spause
Risulta evidente la dipendenza della parte standard da \(\Delta x\). 

Dato che la parte standard non è sempre la stessa per qualsiasi \(\Delta x\) 
infinitesimo non nullo, \(f'(0)\) non esiste.
\end{frame}

\section{La tangente}
\begin{frame}\frametitle{Definizione della tangente e sua caratteristica}
 Sia \(f(x)\) una funzione con pendenza nel punto \(x_0\). Chiamiamo 
 {\color{green!50!black}tangente} a una curva di equazione 
 \(y = f(x)\), in un suo punto \(P\) di ascissa \(x_0\),  la retta per \(P\),
 indichiamola con \(t\), {\color{green!50!black}che ha pendenza uguale alla 
 pendenza della curva in \(P\)}. \\
 \pause
 
 Allora la tangente \(t\) a quella curva nel punto \(P\) avrà equazione 
 {\color{green!50!black}\(t(x) = f’(x_0)(x-x_0)+f(x_0)\)}.
 \spause
 La tangente \(t\) è la retta che meglio approssima la \(f\) nei suoi punti di
 ascissa  infinitamente vicina a  \(x_0\):
 \pause 
 è l'unica retta reale tale che la distanza tra la curva e la tangente 
 nell’infinitamente vicino a \(x_0\), cioè {\color{green!50!black}la differenza 
 \(f(x_0+\Delta x)-t(x_0+\Delta x)\), è un infinitesimo rispetto all'infinitesimo
 \(\Delta x\)}. 
 \end{frame}
 
 
 \begin{frame}\frametitle{Proprietà caratteristica della tangente 1} 
 Infatti:
\begin{align*}
 \frac{f(x_0+\Delta x)-t(x_0+\Delta x)}{\Delta x}&=
 \frac{f(x_0+\Delta x)-(f'(x_0)(\Delta x) +f(x_0))}{\Delta x}=\\
 &=\frac{f(x_0+\Delta x)-f(x_0)}{\Delta x}-f'(x_0).
\end{align*} 

I due termini dell'ultima espressione sono infinitamente vicini perché il secondo
è la parte standard del primo, quindi la loro differenza è un infinitesimo, 
chiamiamolo \(\epsilon\). Perciò, riprendendo l'inizio dell'uguaglianza:
\[
 \frac{f(x_0+\Delta x)-t(x_0+\Delta x)}{\Delta x}=\epsilon.
\]
\end{frame}

\begin{frame}\frametitle{Proprietà caratteristica della tangente 2}
La tangente è l’unica retta con questa caratteristica. \\
\spause
Infatti, supponiamo che una retta  \(r\) passi per il punto \((x_0, f(x_0))\) 
della curva ed abbia coefficiente angolare \(m\). \\
\vspace{3mm}
Se, per ogni infinitesimo \(\Delta x\) diverso da \(0\), \(r\) avesse, 
nei punti di ascissa \(x_0+\Delta x\), una distanza dalla curva pari a un
infinitesimo \(\epsilon\) rispetto a \(\Delta x\), 
\pause
cioè se, per ogni infinitesimo non nullo, fosse: 
\[f(x_0+\Delta x)-(m\cdot \Delta x +f(x_0)) = \epsilon\cdot \Delta x.\]
\pause
allora sarebbe \(\frac{f(x_0+\Delta x)-f(x_0)}{\Delta x} = m+\epsilon\)\\ 
e la parte 
standard di \(\frac{f(x_0+\Delta x)-f(x_0)}{\Delta x}\) sarebbe sempre la stessa.
\spause
Dunque \(f’(x_0)\), avrebbe valore \(m\), perciò sarebbe \(m = f’(x_0)\),
mostrando l’unicità della retta con la caratteristica della tangente.
\end{frame}

\section{L'incremento}
\begin{frame}\frametitle{Come visualizzare le differenze fra \(f\) e \(t\) in
un punto} 
\begin{columns}[T]
 \begin{column}{0.5\textwidth}
 \vspace{-10mm}
  \derivata
 \end{column}
%\hfill
\begin{column}{0.5\textwidth}
 %{\fontsize{10pt}{7.2}\selectfont 
  \only <1>{Sia \(f(x)\) una funzione con pendenza nel punto \(P(x_0, f(x_0))\). 
  Entriamo in  \(P\). }
  \vspace{3mm}
  \only <2>{
  Se esiste \(f'(x_0)\), questa è anche
  la pendenza della tangente \(y=t(x)=f'(x_0)\Delta x+f(x_0)\), 
  che allora c’è, e non è distinguibile dalla curva nel campo visivo del 
  microscopio tarato per vedere l’infinitesimo \(\Delta x\), poiché la
  differenza è un infinitesimo rispetto a \(\Delta x\). \\
 }
 \only<3>{
   Puntando un secondo microscopio infinito nel punto \(P\) all'interno del
   campo visivo del primo microscopio, per poter vedere grandezze dell’ordine
   di un infinitesimo \(\epsilon\) di \(\Delta x\), ancora non si distingue
   la differenza tra la curva e la   tangente, perché si guardano punti le cui
   ascisse in effetti distano da \(x_0\) meno di \(\epsilon \Delta x\) e i
   corrispondenti valori sulla curva e sulla tangente differiscono per un
   infinitesimo dell’infinitesimo \(\epsilon \Delta x\)  considerato.\\}
%  \pause  }

  \only<4->{
%  {\fontsize{10pt}{7.2}\selectfont
  Se invece si punta il secondo microscopio nel primo, ma in corrispondenza di
  \(x_0+\Delta x\), la distanza \(\Delta x\) da \(x_0\) sarà troppo grande per
  essere vista ora. \\
  \vspace {3mm}
  }
  %}
  \only <5>{
  {\color{red}Invece lì si potrà vedere la differenza 
  \(\epsilon \Delta x\)} tra la curva e la sua tangente in \(P\).
  }
  \only <6>{Si è così illustrato il {\color{green!50!black}teorema 
  dell'incremento}: se esiste la pendenza \( f'(x_0)\) della funzione
  \(f\) in \(x_0\), allora, per ogni infinitesimo
  \(\Delta x\): 
  {\color{green!50!black}\(f(x_0+\Delta x)=f'(x_0)\Delta x+ f(x_0)+ 
  \epsilon\cdot \Delta x\)},
  per un opportuno \(\epsilon\), che dipende da \(x_0\) e da \(\Delta x\).
  }
\end{column}
\end{columns}
\end{frame}


\section{Derivata di una funzione}

\begin{frame}\frametitle{Funzione derivata: definizione}
Chiamiamo {\color{green!50!black}derivata della funzione \(f\), e la 
indichiamo con \(f'\)}, la funzione che ad ogni valore \(x\) della variabile
indipendente associa la pendenza della funzione \(f\) in \(x\), se questa c’è.
\spause
Ricordando come era stata definita la pendenza di \(f\) in \(x\), si può anche
dire che la derivata \(f’\) della funzione \(f\) associa a ciascun valore di 
\(x\) la parte standard del rapporto incrementale \(\frac{(f(x+\Delta x)-f(x)}
{\Delta x}\), se questa è sempre la stessa per ogni infinitesimo non nullo 
\(\Delta x\). Cioè:
\[f'(x) = \pst{\dfrac{f(x+\Delta x) - f(x)}{\Delta x}},\]
\pause
%\(f'(x)\) esiste per tutti
per gli \(x\) per i quali la parte standard ci sia e sia sempre la stessa,
 qualunque sia l'infinitesimo non nullo \(\Delta x\).
\end{frame}

\begin{frame}\frametitle{Strategia di calcolo della derivata di un polinomio}
Se \(f\) è un polinomio, il calcolo di \(f'\) prevede nell'ordine:
\spause
\begin{enumerate}[<+->]
 \item esprimere il rapporto incrementale \(\frac{\Delta f(x)}{\Delta x}\);
 \item alcuni sviluppi algebrici tendenti ad eseguire 
 la divisione per \(\Delta x\) e isolare la parte infinitesimale;
 \item l'uso delle proprietà della funzione \(\st\); 
 \item la constatazione che la parte standard del rapporto incrementale c’è 
 ed è sempre la stessa per ogni incremento infinitesimo non nullo della 
 variabile indipendente.
 \end{enumerate}
\end{frame}

\begin{frame}\frametitle {Un esempio}
Per esempio, calcoliamo la derivata di: 
\(f(x) = x^3-5x^2+6x+1\).
\spause
Mediante sviluppi algebrici si va dalla formulazione iniziale del rapporto
incrementale\\
\(\frac{\Delta x}{\Delta x}=\frac{(x+\Delta x)^3-5(x+\Delta x)^2+6(x+\Delta x)+1)
-(x^3-5x^2+6x+1)}{\Delta x}= \dots\)\\
alla sua opportuna formulazione come:\\
 \(\dots= 3x^2-10x+6+3x\Delta x-\Delta x\)
\quad per qualsiasi infinitesimo \(\Delta x\) non nullo.
\spause
Si applica la funzione \(\st\), con le sue proprietà, per ottenere:\\
\(\pst{\frac{\Delta f}{\Delta x}}=3x^2-10x+6\),\\
e notare che non dipende da \(\Delta x\).
\spause
Quindi: \(f'(x)=3x^2-10x+6\), per ogni \(x\) reale.
\end{frame}


\begin{frame}\frametitle{Come evitare complicazioni}
Con polinomi di grado maggiore il calcolo algebrico può diventare molto 
laborioso.
\spause
Le funzioni usate nelle applicazioni scientifiche e tecniche spesso sono 
generate da un piccolo nucleo di funzioni iniziali e da poche operazioni su 
funzioni.
\spause 
Le derivate di tali funzioni dunque si ottengono conoscendo
le derivate delle funzioni iniziali e come ottenere le derivate di ciò che 
risulta applicando un’operazione tra funzioni, avendo a disposizione le derivate 
delle singole funzioni su cui si opera.
\spause
Otterremo le derivate di alcune funzioni elementari e giustificheremo i 
risultati della derivazione delle leggi di composizione di funzioni, ottenute 
mediante le usuali operazioni su funzioni. Lasceremo i casi semplici come esercizio.
\end{frame}

\section{Derivata di funzioni elementari}

\begin{frame}\frametitle{Es.1: derivata della funzione costante}
Proponiamo ai corsisti di applicare quanto hanno appreso a questi primi casi 
elementari. Discuteremo le soluzioni nella prossima lezione collettiva.\\
\begin{columns}[T]
\begin{column}{.4\textwidth}
 \begin{center}
\diffcostante  
 \end{center}
\end{column}
\begin{column}{.6\textwidth}
% {\footnotesize 
 \begin{align*}
 f(x)&=k.\\%[1em]
 &\pst{\dfrac{f(x+\Delta x) - f(x)}{\Delta x}} =\\
&= \dots\
 %       &=\pst{\dfrac{k -k}{\Delta x}} = \\
%       &=\pst{\dfrac{0}{\Delta x}} = \pst{0}=0.
 \end{align*} 
\end{column}
\end{columns}
Concludendo:\\
\dots
\end{frame}

\begin{frame}\frametitle{Es.2: derivata della funzione identica}
\begin{columns}[T]
\begin{column}{.6\textwidth}
 \begin{center}
\diffbisettrice  
 \end{center}
\end{column}
\begin{column}{.5\textwidth}
{\footnotesize 
\begin{align*}
f(x)&=x.\\%[1em]
& \pst{\dfrac{f(x+\Delta x) - f(x)}{\Delta x}} =\\
&= \dots\
% &=\pst{\dfrac{\cancel{+x}+\Delta x ~\cancel{-x}}{\Delta x}} = \\
% &=\pst{\dfrac{\cancel{\Delta x}}{\cancel{\Delta x}}} = \\
% &=\pst{1}=1.
\end{align*}  }
\end{column}
\end{columns}
% L'incremento \(\Delta f\) è uguale all'incremento infinitesimo non nullo 
% \(\Delta x\), quindi il loro rapporto vale \(1, ~\forall \Delta x \ne 0\).
Concludendo:\\
\dots

NB: \(\Delta f\) e \(\Delta x\) hanno valore uguale, ma non hanno lo stesso
significato.
\end{frame}

\begin{frame}\frametitle{Es.3: derivata della funzione reciproca}
%{\footnotesize
\begin{align*}
f(x)&=\frac{1}{x},\quad \text{per } x\ne 0.\\[.5em]
&\pst{\dots}=\dots\\
% \dfrac{f(x+\Delta x) - f(x)}{\Delta x}} =
%    \pst{\dfrac{\frac{1}{x+\Delta x}-\frac{1}{x}}{\Delta x}} = \\
% &=\pst{\dfrac{\cancel{+x}~\cancel{-x}-\Delta x}
%              {\Delta x \tonda{x^2+x \Delta x}}} \overset{(*)}{=} 
%    \pst{\dfrac{\cancel{-\Delta x}}
%              {\cancel{\Delta x} \tonda{x^2+x \Delta x}}} \overset{(**)}{=}\\ 
% &\overset{(**)}{=}\frac{\pst{-1}}{\pst{x^2+x \Delta x}}
%             =-\frac{1}{x^2}.
\end{align*}\\
\vspace{2.5cm}
{\footnotesize Individua i passaggi in cui sono necessarie le note seguenti :\\
\((*)\) \(\forall \Delta x \ne 0\), infinitesimo;\quad
\((**)\) \(\pst{\frac{a}{b}}=\frac{\pst{a}}{\pst{b}}\), purché\dots}

Concludendo: \dots
\end{frame}


\begin{frame}\frametitle{Derivata della funzione radice quadrata (1)}
\begin{align*}
f(x)&=\sqrt{x}\text{,\quad per }x\geq 0. \\[1em]
&\pst{\dfrac{f(x+\Delta x) - f(x)}{\Delta x}} =\\
&=\pst{\dfrac{\sqrt{x+\Delta x}-\sqrt{x}}{\Delta x}}  
\overset{(*)}{=}\dfrac{\pst{\sqrt{x+\Delta x}}-\pst{\sqrt{x}}}
             {\pst{\Delta x}} 
             \only <3->{\overset{(**)}{=} \\
&\overset{(**)}{=}\dfrac{\sqrt{\pst{x+\Delta x}}-\sqrt{\pst{x}}}{\pst{\Delta x}} 
= 
\dfrac{\cancel{+\sqrt{x}}~\cancel{-\sqrt{x}}}{0} = \dots } =
\end{align*}
\only <2->{\((*)\) Questo passaggio è consentito? NO! Ma vediamo ugualmente 
cosa succederebbe se lo accettassimo.\\}
\only <3->{\((**)\) Ora si applica il fatto che \(\pst{\sqrt{x}}=\sqrt{\pst{x}}\).}
\end{frame}

\begin{frame}\frametitle{Derivata della funzione radice quadrata (2)}
Sembrerebbe di non poter derivare la funzione radice quadrata.
\pause

Razionalizziamo, invece, il numeratore:
{\fontsize{10pt}{7.2}\selectfont
\begin{align*}
f(x)&=\sqrt{x}\text{,\quad per }x\geq 0.\\[.5em]
& \pst{\dfrac{f(x+\Delta x) - f(x)}{\Delta x}} =
\pst{\dfrac{\sqrt{x+\Delta x}-\sqrt{x}}{\Delta x} \cdot
       \dfrac{\sqrt{x+\Delta x}+\sqrt{x}}{\sqrt{x+\Delta x}+\sqrt{x}}} = \\
&=\pst{\dfrac{\cancel{+x}+\Delta x~\cancel{-x}}
             {\Delta x \tonda{\sqrt{x+\Delta x}+\sqrt{x}}}}= 
  \pst{\dfrac{\cancel{\Delta x}}
             {\cancel{\Delta x} \tonda{\sqrt{x+\Delta x}+\sqrt{x}}}}= \\
&=\dfrac{\pst{1}}{\pst{\sqrt{x+\Delta x}}+\pst{\sqrt{x}}}= 
  \dfrac{1}{\sqrt{\pst{x+\Delta x}}+\sqrt{\pst{x}}} = \\
&=\dfrac{1}{\sqrt{x}+\sqrt{x}} = \frac{1}{2\sqrt{x}}.
\end{align*}
A voi il compito di completare l’esempio e di giungere alla derivata.
}
\end{frame}

\section{Conclusioni}

\begin{frame}\frametitle{Riassunto}
\begin{enumerate} %[<+->]
\item 
Se c'è la parte standard del rapporto incrementale, per un fissato \(x_0\), e 
se il suo valore è sempre uguale per qualsiasi infinitesimo
non nullo \(\Delta x\), allora la pendenza della funzione in quel punto esiste
e \(f'(x_0) = \pst{\frac{f(x_0+\Delta x) - f(x_0)}{\Delta x}}\).
\item 
La derivata \(f'\) di una funzione \(f\) è una funzione che associa ad ogni 
punto della curva di \(f\) la pendenza di \(f\) in quel punto, se c'è.
\item La tangente è la retta che nel punto di tangenza ha pendenza uguale
alla pendenza della curva.
\item Grazie a due microscopi opportunamente posizionati uno nell'altro si
visualizza la differenza fra una curva e la sua tangente. La differenza risulta
essere un infinitesimo dell'infinitesimo \(\Delta x\).
\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Se tutto questo sembra promettente\dots}
Possiamo:
\begin{itemize}
\item affrontare nei dettagli alcuni fondamentali teoremi sulla derivazione.
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Il tuo contributo}
Per approfondire, affronta alcune delle seguenti questioni.
\begin{enumerate}
\item Esiste la pendenza di \(f(x)=\sqrt{\abs{x}}\) per \(x=0\)?
\item Completa le slide sulla derivata delle prime funzioni elementari
% \item Dimostra che il coefficiente angolare della tangente è necessariamente 
% \(f'(x_0)\).
\item Deduci il teorema dell'incremento dalla definizione non standard di 
derivata.
\item Confronta la versione non standard del teorema dell'incremento con 
quella standard.
\item Spiega perché non è consentito il passaggio contrassegnato con \((*)\) 
nel derivare la funzione radice quadrata.
% \item Dimostra anche la nota successiva della stessa slide:
% \(\pst{\sqrt{x}}=\sqrt{\pst{x}}\).
\item Sapresti trarre una definizione non standard di continuità di una funzione
in un suo punto nel quale la funzione è derivabile?
\item \dots
\end{enumerate}
\end{frame}
\begin{comment}
\end{comment}
