%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        Corso Analisi Non Standard - Verona 2020
%
%                      Gruppo NSA-Verona
%
%                  Dai razionali agli iperreali
%
%                             testo
%
% This work may be distributed and/or modified under the
% conditions of the Creative Commons BY-SA
%
% The Current Maintainer of this work is 
% Daniele Zambelli - daniele.zambelli@gmail.com
%
% Copyright 2020-24 Daniele Zambelli
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Infinitesimi e infiniti}

\begin{frame}\frametitle{Nomenclatura}
La scelta di avere più elementi separatori di una sezione di razionali porta 
come conseguenza di avere dei numeri, in valore assoluto, minori di 
qualunque numero razionale positivo.
% 
% Abbiamo definito {\color{green!50!black}infinitesimo} un numero 
\spause
Usando i naturali, diremo che:
{\color{green!50!black}
\begin{center}
\(\epsilon\) è infinitesimo se: \quad
\(\abs{\epsilon} < \dfrac{1}{n} \quad  \forall n \in \Nz\)
\end{center}
}
Lo zero è l’unico razionale infinitesimo. 
\spause
L’ipotesi che ci sia più di un elemento separatore comporta che ci siano 
molti infinitesimi, anzi infiniti (almeno tutti i sottomultipli naturali di un 
infinitesimo non nullo).
\end{frame}

\begin{frame}\frametitle{Un nuovo predicato}
La precedente definizione è una pseudo definizione perché fa riferimento alla 
{\color{orange!50!black}totalità dei numeri naturali}, 
ma noi non sappiamo cosa sia ``la totalità dei numeri naturali''.
\spause
Dobbiamo quindi introdurre nel linguaggio un 
{\color{blue!70!black}nuovo predicato} che dica esplicitamente che un numero 
è infinitesimo. 
\spause
Per operare con un valore \(\epsilon\) infinitesimo dovremo in qualche modo 
dichiarare che \(\epsilon\) è infinitesimo.
\end{frame}

\begin{frame}\frametitle{Infinitesimi e assolutamente trascurabili}
Anche 
{\color{blue!70!black}un multiplo di un infinitesimo è un infinitesimo}. 
\spause[.5]
Indicato con \(\epsilon\) un infinitesimo positivo e aggiungendo 
\(\epsilon\) a sé stesso un numero \(m \neq 0\) di volte si ha che:\quad
\(m \times {\epsilon} < \dfrac{1}{n} \quad \forall n \in \Nz\)
\spause[.5]
infatti ciò è equivalente a: \quad 
\(\epsilon < \dfrac{1}{m \times n}\)
\spause[.5]
ma essendo \(\epsilon\) minore di qualunque reciproco di numero naturale, 
sarà anche minore di \(\dfrac{1}{m \times n}\) dato che:
\quad \(m \times n \in \Nz\).
\spause[.5]
Quindi a differenza dei numeri 
{\color{orange!50!black}praticamente trascurabili}, un infinitesimo è 
{\color{green!50!black}assolutamente trascurabile} poiché, sommandolo a se 
stesso un numero 
naturale inteso arbitrario di volte, rimane infinitesimo.
\spause[.5]
Un infinitesimo è quindi un numero assolutamente trascurabile e\\
\pause
un numero assolutamente trascurabile è un infinitesimo.
\end{frame}

\section{Iperreali}

\begin{frame}\frametitle{Individuabilità degli infinitesimi}
Gli infinitesimi non sono distinguibili gli uni dagli altri mediante 
l’interposizione di un razionale inteso, cioè mediante qualcosa che si riesca 
ad apprezzare.
\spause
Sarà possibile distinguere un infinitesimo da un altro solo se:
\begin{itemize}
\item <2->
il secondo è ottenuto dal primo in modo conosciuto;
\item <3->
sono dichiarati come uguali o diversi.
\end{itemize}

\vspace{1em}
\only <4->{
Ad es. \\
Se \(\alpha \text{~e~} \beta\) sono due infinitesimi non posso dire se 
\(\alpha \text{~sia minore, uguale o maggiore di~} \beta\)
anche se, certamente: 
\[\alpha < \beta \quad \text{o} \quad
  \alpha = \beta \quad \text{o} \quad
  \alpha > \beta\] 

\pause
Ma, se so anche che 
\(\delta \text{~è un infinitesimo positivo e~} \beta=\alpha+\delta\), 
allora posso dire che \(\alpha \text{~è minore di~} \beta\).
}
\end{frame}

\begin{frame}\frametitle{Iperreali (1)}
Affinché i molti elementi separatori per ciascuna sezione dei razionali siano 
opportuni, si vuole che i nuovi enti inventati si comportino come numeri, 
cioè che si strutturino in un campo ordinato che estenda quello dei 
razionali, in cui l’ordine, l’addizione e la moltiplicazione abbiano le 
usuali proprietà.
\spause
\begin{center}
{\color{green!50!black}Tutti questi enti formeranno l'insieme degli 
iperreali}
\end{center}
\spause
Questo insieme include i {\color{green!50!black}razionali} perché 
ciascun razionale \(q\) è ele­mento separatore (eventualmente assieme ad 
altri) della sezione dei razionali costituita dall’insieme dei razionali che 
sono minori di \(q\) e da quello dei razionali che sono maggiori di \(q\). 
\end{frame}

\begin{frame}\frametitle{Iperreali (2)}
Questo nuovo insieme numerico include anche gli 
{\color{green!50!black}infinitesimi} perché questi 
sono gli elementi separatori della sezione costituita dall’insieme dei 
razionali negativi e dall’insieme dei razionali positivi.
\spause
L'insieme di tutti gli infinitesimi forma la monade dello zero.
\pause
L'unico numero razionale infinitesimo è zero.
\spause
Se esiste un infinitesimo allora ne esistono infiniti: anche tutti i suoi 
sottomultipli e le sue potenze.
\spause
Ma anche tutti i suoi multipli, come abbiamo visto.
\end{frame}

\begin{frame}\frametitle{Strutturazione degli infinitesimi}
Volendo giungere a un campo ordinato, nell’inventare gli infinitesimi è 
opportuno dotarli di:
\begin{itemize}[<+->]
\item un {\color{green!50!black}ordine} lineare (per dire, nel confronto, chi 
è maggiore);
\item una {\color{green!50!black}addizione} (per poter rappresentare un 
aggiungere);
\item una {\color{green!50!black}moltiplicazione} (che permetta di tener 
conto delle ripetizioni di aggiunte);
\item degli elementi {\color{green!50!black}reciproci} degli infinitesimi 
diversi da zero.
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Infiniti}
Si noti che, per il reciproco di un infinitesimo non nullo, succede che:
{\color{blue!50!black}
\[\abs{\epsilon} < \frac{1}{n} \quad \forall n \in \Nz \sRarrow 
\abs{\frac{1}{\epsilon}} > n \quad \forall n \in \Nz\]
}
\pause
Così, per giungere a un campo ordinato, dobbiamo introdurre dei numeri 
maggiori di qualunque numero naturale (o razionale): sono numeri 
{\color{green!50!black}infiniti}.
\spause[.3]
Tutti i multipli di un infinito sono infiniti e anche tutti i suoi 
sottomultipli.
Poiché gli infiniti sono reciproci degli infinitesimi diversi da zero, tra 
gli iperreali ci sono anche {\color{green!50!black}infiniti infiniti}.
\spause[.3]
Chiameremo infiniti {\color{green!50!black}positivi} i reciproci degli 
infinitesimi positivi e 
infiniti {\color{green!50!black}negativi} i reciproci degli infinitesimi 
negativi. 
\spause[.3]
Gli infiniti erediteranno {\color{green!50!black}ordine e operazioni} che 
provengono dai loro reciproci.
\spause[.3]
Gli infiniti {\color{blue!50!black}non possono essere elementi separatori di 
alcuna sezione dei razionali} perché avrebbero una delle due classi vuota.
\end{frame}

\begin{frame}\frametitle{Iperreali}
Si sa che le sezioni dei razionali possono essere organizzate con un ordine e 
con le operazioni ereditate dai razionali in modo da costituire un 
{\color{green!50!black}campo ordinato}.
\spause
L’aver strutturato come si è fatto gli {\color{green!50!black}infinitesimi} 
permetterà di organizzare adeguatamente anche gli elementi separatori delle 
sezione dei razionali perché, nella stessa sezione, differiscono tra loro di 
un infinitesimo. 
\spause
Finalmente, aggiungendo anche gli {\color{green!50!black}infiniti} con il 
loro ordine e le loro operazioni, e considerando anche il risultati delle 
operazioni tra infiniti e non infiniti, si otterrà un 
{\color{green!50!black}campo ordinato} che viene chiamato il 
{\color{green!50!black}campo degli iperreali}, che viene denotato con 
{\color{green!50!black}\(\IR\)}.
\end{frame}

\begin{frame}\frametitle{Tipi di Iperreali (1)}
Siamo partiti cercando di determinare gli elementi separatori delle sezioni 
di numeri razionali e ci ritroviamo con un nuovo insieme numerico piuttosto 
affollato.

È utile raggruppare gli iperreali nelle seguenti categorie:
\pause
\vspace{-4mm}
\begin{columns}[c]
\begin{column}{.58\textwidth}
\begin{itemize}
\item {\color{orange!50!black}[f] finiti:}
\begin{itemize}
\item {\color{green!50!black}[i] infinitesimi:}
\begin{itemize}
\item {\color{blue!80!black}[0] zero;}
\item {\color{green!80!black}[inn] infinitesimi non nulli;}
\end{itemize}
\item {\color{orange!50!black}[f] finiti non infinitesimi;}
\end{itemize}
\item {\color{red!50!black}[I] infiniti.}
\end{itemize}
\end{column}%
% \hfill%
\begin{column}{.38\textwidth}
\begin{center} \iperrealiset \end{center}
\end{column}%
\end{columns}
\end{frame}

\begin{frame}\frametitle{Tipi di iperreali (2)}
Riassumendo quanto visto, diamo le seguenti definizioni di sottoinsiemi  di 
numeri iperreali.
\spause
\begin{description} [<+->]
\item [infinitesimo:]
Diciamo infinitesimo un numero \(a\) tale che:
\[\abs{a} < \frac{1}{n} \quad \forall n \in \Nz\]
\item [finiti:]
Diciamo finito un numero \(a\) tale che:
\[\exists n \in \N \quad \abs{a} < n\]
\item [infiniti:]
Diciamo infinito un numero \(a\) tale che:
\[\abs{a} > n \quad \forall n \in \N\]
\end{description}
\end{frame}

\begin{frame}\frametitle{Convenzioni}
Nel resto del corso, dove non specificato in modo diverso seguiremo queste 
convenzioni:
\begin{center}
\begin{columns} [T]
\begin{column}{.35\textwidth}
\emph{tipo}\\
zero\\
infinitesimo\\
infinitesimo non nullo\\
finito non infinitesimo\\
finito\\
infinito\\
qualsiasi\\
\end{column}
\begin{column}{.05\textwidth}
\emph{sigla}\\
~\\
\emph{i}\\
\emph{inn}\\
\emph{fni}\\
\emph{f}\\
\emph{I}\\
~\\
\end{column}
\begin{column}{.10\textwidth}
\emph{simboli}\\
\(0\)\\
\(\alpha, \beta, \gamma, \delta, \dots\)\\
\(\alpha, \beta, \gamma, \delta, \dots\)\\
\(a, b, c, d, \dots\)\\
\(a, b, c, d, \dots\)\\
\(A, B, C, \dots\)\\
\(x, y, z, \dots\)\\
\end{column}
\end{columns}
\end{center}
\end{frame}

\section{Parte standard}

\begin{frame}\frametitle{Sezioni di razionali e numeri reali}
Come abbiamo già visto, le diverse sezioni di numeri razionali individuano 
delle classi di equivalenza formate dai loro elementi separatori.

Scegliamo \emph{un} rappresentante per ogni sezione di razionali in base ai 
seguenti criteri:
\begin{enumerate}
\item <2->
Se nella classe di equivalenza c’è un {\color{blue!70!black}razionale} (che 
deve essere unico), il rappresentante sarà il razionale. 
\item <3->
Se una classe di equivalenza è relativa a una sezione di razionali che 
approssimano {\color{blue!70!black}una grandezza con certe proprietà}, il 
rappresentante dovrà avere le corrispondenti proprietà. 
\item <4->
Altrimenti un rappresentante a piacere.
\end{enumerate}
\only <5->{
L’elemento scelto a rappresentare la classe di equivalenza viene detto 
{\color{green!50!black}numero reale}.}
\end{frame}

\begin{frame}\frametitle{\(f: \IR \mapsto \R\)}
Ogni numero iperreale finito appartiene ad una monade che ha al suo interno 
un numero reale scelto con i criteri esposti sopra.
\spause
\begin{center}
{\color{orange!50!black}Questo numero reale è unico.}
\end{center}
\spause
Quindi ogni numero {\color{orange!50!black}iperreale finito} ha 
{\color{orange!50!black}un solo} numero reale infinitamente vicino a sé.
\spause
Questo ci suggerisce una funzione che ha come insieme di definizione gli 
iperreali finiti e come insieme immagine i reali e che associa ad ogni 
iperreale finito il reale a lui infinitamente vicino.
\end{frame}

\begin{frame}\frametitle{Parte standard}
Chiamiamo Parte Standard (\(\pst{}\)) la funzione che associa a un numero 
iperreale finito l'unico numero reale a lui infinitamente vicino.
\spause
In simboli:
\[x \in \IR \sand a \in \R \sand a \approx x \sRarrow \pst{x}=a\]
Dove \(\approx\) significa ``infinitamente vicino''.
\spause
O, usando le solite convenzioni:
\[x \in \IR \sand a \in \R \sand x=a+\epsilon \sRarrow \pst{x}=a\]
Dove \(\epsilon\) è un infinitesimo.
\end{frame}

\begin{frame}\frametitle{Soluzioni di problemi}
Nessuno userà mai un numero iperreale o un numero reale: \\
in qualunque 
attività umana si usa solo un piccolissimo sottoinsieme dei numeri razionali.
\spause
Abbiamo inventato vari insiemi numerici per semplificare la soluzione di 
problemi. 
Il flusso risolutivo è il seguente:
\begin{enumerate} [<+->]
\item ho un problema con dati razionali (misure);
\item trasformo questi valori in numeri iperreali;
\item risolvo il problema;
\item trasformo la soluzione in numero reale usando la funzione Parte 
Standard;
\item ricavo un'approssimazione razionale utilizzabile.
\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Utilità dei nuovi numeri}
Ma se le misure sono razionali e i risultati che posso utilizzare sono 
razionali, perché inventare altri numeri?
\begin{itemize} [<+->]
\item perché permettono di generalizzare certi problemi;
\item perché permettono di trovare nuove soluzioni;
\item perché permettono di inventare nuovi calcoli simbolici;
\item perché semplificano enormemente la soluzione di certi problemi e, nella 
maggior parte dei casi, i costi dovuti alle traduzioni numeriche sono 
insignificanti.
\end{itemize}
\end{frame}


\section{Retta e strumenti ottici}

\begin{frame}\frametitle{Retta e iperreali}
La retta è un potente supporto per la rappresentazione dei numeri iperreali.
\spause
Per poter visualizzare sulla retta infinitesimi, numeri a distanza 
infinitesima o infiniti, utilizzeremo dei particolari strumenti ottici:
\begin{enumerate}
\item microscopio
\item telescopio
\item grandangolo
\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Microscopio non standard}
Il {\color{green!50!black}microscopio non standard} permette di applicare un 
{\color{blue!50!black}ingrandimento infinito} ad una porzione di piano.
\spause[.5]
In pratica ci permette di entrare all'interno di un punto, di una porzione di 
spazio infinitamente piccola.
\spause[.5]
Un punto di curva liscia, ingrandito da un microscopio non standard 
appare un segmento. L'unità di misura nel campo visivo è \(\epsilon\).
\pause

\begin{center}
\begin{columns} [T]
\begin{column}{.29\textwidth}
\microstandard

\emph{\centering \footnotesize Visualizza \(5,001\)}
\end{column}
\begin{column}{.29\textwidth}
\micrononstandard
\emph{\centering \footnotesize Visualizza \(2 -3 \epsilon\)}
\end{column}
\begin{column}{.40\textwidth}
\microcurva

\vspace{3mm}
\emph{\centering \footnotesize Un tratto infinitesimo di curva}
\end{column}
\end{columns}
\end{center}
\end{frame}

\begin{frame}\frametitle{Telescopio non standard}
Il {\color{green!50!black}telescopio non standard} avvicina una porzione di 
piano, {\color{blue!50!black}infinitamente distante}, senza modificare la 
scala.
\spause[.5]
In pratica ci permette di spostare la vista lungo un asse, di una quantità 
finita o infinita.
% \spause[.5]
% Una porzione di curva ingrandita con un microscopio non standard appare 
% come un segmento.
\pause

\begin{center}
\begin{columns} [T]
\begin{column}{.29\textwidth}
\telestandard
\begin{center}\emph{\footnotesize Visualizza \(127034\)}\end{center}
\end{column}
\begin{column}{.29\textwidth}
\telenonstandard
\vspace{-2em}
\begin{center}\emph{\footnotesize Visualizza \(A\)}\end{center}
\end{column}
\end{columns}
\end{center}
\end{frame}

\begin{frame}\frametitle{Grandangolo non standard}
Il {\color{green!50!black}grandangolo non standard} (zoom out) non standard 
{\color{green!50!black}riduce di un fattore infinito la scala} di 
visualizzazione.
\spause[.5]
In pratica ci permette di ampliare il tratto visibile.
\pause

\begin{center}
\begin{columns} [T]
\begin{column}{.29\textwidth}
\grandstandard
\begin{center}\emph{\footnotesize Riduce di un fattore \(100\)}\end{center}
\end{column}
\begin{column}{.29\textwidth}
\grandnonstandard
\vspace{-2em}
\begin{center}\emph{\footnotesize Riduce di un fattore \(A\)}\end{center}
\end{column}
\end{columns}
\end{center}
\end{frame}

\section{Conclusioni}

\begin{frame}\frametitle{Riassunto}
\begin{enumerate} %[<+->]
\item 
Si sono inventati gli {\color{green!50!black}infinitesimi diversi da zero} 
che sono numeri minori, in valore assoluto, di qualunque razionale: ce ne 
sono infiniti.
\item 
Per distinguerli dobbiamo creare un apposito {\color{green!50!black}
predicato}. 
\item 
Creiamo un insieme numerico che sia un {\color{green!50!black}campo ordinato} 
che contenga anche gli infinitesimi: gli iperreali.
\item 
Questo campo dovrà contenere anche gli {\color{green!50!black}infiniti}, ce 
ne saranno infiniti.
\item 
Possiamo rappresentare gli iperreali dotando la retta di particolari 
strumenti ottici: {\color{green!50!black}microscopi, telescopi, grandangoli}.
\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Se tutto questo sembra promettente\dots}
Dobbiamo precisare come:
\begin{itemize}
\item eseguire le operazioni;
\item effettuare il confronto.
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Il tuo contributo}
Per approfondire, affronta alcune delle seguenti questioni.
\begin{enumerate}
\item Quale differenza c'è tra numeri iperreali finiti e numeri reali?
\item Crea alcune situazioni in cui si debba utilizzare una 
combinazione di strumenti ottici standard e non standard per visualizzare 
numeri iperreali.
\item Dimostra che una monade non può contenere numeri reali diversi.
\item Perché i sottomultipli di un infinito sono anch'essi infiniti?
\item Nella visione con grandangolo non standard della diapositiva 23, dove 
si trovano i reali?
\item Inventa un problema e prova a risolverlo.
\end{enumerate}
\end{frame}

      %--------------------------------------------------------------%

\appendix 
