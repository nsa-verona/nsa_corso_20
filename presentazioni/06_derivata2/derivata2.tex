%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        Corso Analisi Non Standard - Verona 2020
%
%                      Gruppo NSA-Verona
%
%                         Derivata 2
%
%                           testo
%
% This work may be distributed and/or modified under the
% conditions of the Creative Commons BY-SA
%
% The Current Maintainer of this work is 
% Daniele Zambelli - daniele.zambelli@gmail.com
%
% Copyright 2020-24 Daniele Zambelli
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Derivata della somma}

\begin{frame}[shrink= 3]
\frametitle{Derivata della somma}
Vedremo come, usando l’analisi non standard, si può dimostrare facilmente la
correttezza delle regole di derivazione. 
\spause

In questo modo si possono motivare e giustificare queste regole anche nella 
didattica delle scuole superiori già dal terzo anno, cosa che normalmente non 
avviene.\\

Spesso, anche negli anni successivi di alcuni corsi di studi, ci si limita alla
sola enunciazione formale ed applicazione meccanica di tali regole, a causa
delle notevoli difficoltà concettuali delle dimostrazioni classiche.
\spause

Per cominciare, si dimostri la correttezza della regola della derivata della
funzione somma di due funzioni.\\

Siano \(g\) e \(h\) funzioni derivabili in \(c\) e sia \(F=g+h\).
Allora esiste la derivata  \(F'\) in \(c\) e  \(F'(c)= g'(c)+ h'(c)\).\\
La dimostrazione, che segue direttamente dalle definizioni delle nozioni 
considerate, viene lasciata come utile esercizio e sarà discussa nella prossima
lezione in sincrono.\\
\end{frame}


\section{Derivata del prodotto}
\begin{frame}\frametitle{Derivata del prodotto}
 \only <1>{Siano \(g, h\) funzioni derivabili in un certo valore, diciamo \(x\), 
 allora anche \(F=g\times h\) è derivabile in \(x\) e la sua derivata è  
 \(F'(x)=g'(x)\times h(x)+g(x)\times h'(x)\). \\
 Infatti, per ogni infinitesimo \(\Delta x\ne 0\):}

 {\fontsize{8pt}{7.2}\selectfont
 \begin{align*}
\only <1->{&\pst{\frac{F(x+\Delta x) - F(x)}{\Delta x}} =
\pst{\frac{g(x+\Delta x)\times h(x+\Delta x) - g(x)\times h(x)}{\Delta x}}=\\}
\only <2> {& \text{si toglie e si aggiunge \(g(x)\times h(x+\Delta x)\) al fine
di sfruttare i rapporti incrementali,}\\
&\text{ di cui si dispone perché per ipotesi \(g\) e \(h\) sono derivabili, 
e si ottiene:}\\}
\only <2->{&=\pst{\frac{g(x+\Delta x)\times h(x+\Delta x)-g(x)\times h(x+\Delta x) 
     +g(x)\times h(x+\Delta x) - g(x)\times h(x)}{\Delta x}} =\\}
\only <3-> {&=\pst{\frac{g(x+\Delta x)\times h(x+\Delta x)-g(x)
      \times h(x+\Delta x)}{\Delta x}
     +\frac{g(x)\times h(x+\Delta x) - g(x)\times h(x)}{\Delta x}}=\\
&\overset{(2)}{=}\pst{\frac{g(x+\Delta x)\times h(x+\Delta x)-g(x)\times h(x+\Delta x)}
  {\Delta x}} +\pst{\frac{g(x)\times h(x+\Delta x) - g(x)\times h(x)}{\Delta x}}=\\}
\only <4-> {&=\pst{\frac{g(x+\Delta x)-g(x)}{\Delta x}\times h(x+\Delta x)} +
    \pst{g(x)\times \frac{h(x+\Delta x) - h(x)}{\Delta x}}=\\
&\overset{(3)}{=}\pst{\frac{g(x+\Delta x)-g(x)}{\Delta x}}\times\pst{h(x+\Delta x)} +
    \pst{g(x)}\times \pst{\frac{h(x+\Delta x) - h(x)}{\Delta x}};\\}
 \only< 5->{&\text{\(h\) è derivabile, quindi \(h(x+\Delta x) - h(x)\)
 deve essere un infinitesimo, per cui}\\
 &\text{\(\pst{h(x+\Delta x)}=\pst{h(x)}=h(x)\). I rapporti incrementali che
 compaiono hanno sempre }\\
 &\text{la stessa parte standard, pertanto la loro parte standard è la loro
 derivata in \(x\)}\\
 &\text{e si può concludere:}} 
 \end{align*}
\only<6->{\begin{center} \(F'(x)= g'(x)\times h(x)+ g(x)\times h'(x).\) \end{center}}

\only <3>{(2) Dato che \(\pst{a+b}=\pst{a}+\pst{b}\). }
\only <4>{(3) Dato che \(\pst{a\times b}=\pst{a}\times\pst{b}\).}
 }
\end{frame}


\begin{frame}[shrink=3]
\frametitle{Incremento di un rettangolo incrementando i lati}
Spesso si rappresenta il prodotto \(ab\) con l'area di un rettangolo
di dimensioni \(a\) e \(b\).
Incrementando il lato di lunghezza \(a\) di una quantità \(\Delta a\)
e il lato di lunghezza \(b\) di una quantità \(\Delta b\), l’area del rettangolo
aumenta con l’aggiunta di tre rettangoli, come illustrato col colore blu nel
disegno, uno di area \(a\times \Delta b\), uno di area \(b\times \Delta a\) e
uno di area \(\Delta a\times \Delta b\).
\pause
\begin{columns} [T]
\begin{column}{.38\textwidth}
\vspace{8.8mm}
\begin{center} 
\incrementaleprodotto 
\footnotesize{\emph{Incrementi \(\Delta b \text{~e~} \Delta h\), finiti non 
infinitesimi}}
\end{center}
\end{column}%
% \hfill%
\begin{column}{.58\textwidth}
\vspace{-7mm}
\begin{center} 
\differenzialeprodotto 
\vspace{4.2mm}
\footnotesize{\emph{Incrementi \(\Delta b \text{~e~} \Delta h\), infinitesimi}}
\end{center}
\end{column}%
\end{columns}
\spause[.5]
Che quelle indicate siano le misure di vari rettangoli dipende dalla 
proprietà distributiva applicata al prodotto dei binomi \(a+\Delta a\) e 
\(b+\Delta b\).
% L'incremento dell'area del rettangolo dipende dalle dimensioni del rettangolo 
% e dagli incrementi dei lati, secondo una regola che applichiamo anche a 
% incrementi infinitesimi:
\[\Delta A = b \times \Delta a + a \times \Delta b + \Delta b \times \Delta a.\]
\end{frame}

\begin{frame}\frametitle{Derivata del prodotto: una diversa giustificazione}
{\fontsize{10pt}{7.2}\selectfont
Sia \(F=g\times h\). Chiamiamo \(\Delta F,~\Delta g,~\Delta h\) gli incrementi
delle funzioni \(F\), \(g\) e \(h\), in corrispondenza dell'incremento 
infinitesimo non nullo \(\Delta x\) della variabile indipendente.
Poiché \(g\) e \(h\) sono  derivabili, \(\Delta g\) e \(\Delta h\) sono 
incrementi infinitesimi. 
\pause
Applicando l'ultima formula della slide precedente,}\\
{\fontsize{10pt}{7.2}\selectfont
\(\pst{\dfrac{\Delta F}{\Delta x}} =
\pst{\dfrac{g(x+\Delta x)\times h(x+\Delta x)-g(x)\times h(x)}{\Delta x}}=\)\\
\(=\pst{\frac{\Delta g \times h + g \times \Delta h + \Delta g \times \Delta h}
             {\Delta x}} =\)
che, esplicitando i termini, diventa \\}
{\fontsize{9pt}{7.2}\selectfont
\(= \pst{\frac{(g(x+\Delta x)-g(x)) \times h(x) + g(x) \times (h(x+\Delta x)-h(x))
 +(g(x+\Delta x)-g(x)) \times (h(x+\Delta x)-h(x))}
             {\Delta x}}=\)}\\
{\fontsize{10pt}{7.2}\selectfont
\(=\pst{\dfrac{\Delta g}{\Delta x} \times h + g \times \dfrac{\Delta h}{\Delta x} + 
       \dfrac{\Delta g \times \Delta h}{\Delta x}} = \) }
\end{frame}

\begin{frame}[shrink=6]
\frametitle{Derivata del prodotto: variazioni sul tema}
\[=\pst{\dfrac{\Delta g}{\Delta x}} \times\pst{ h} +
\pst{g} \times \pst{\dfrac{\Delta h}{\Delta x}} + 
  \pst{\dfrac{\Delta g \times \Delta h}{\Delta x}}.
\]
Valutando le parti standard grazie alle ipotesi su \(g\) e \(h\), otteniamo che
il contributo dei primi due addendi è \(g’\times h+g\times h’\).\\

Rimane solo da valutare:
\(\pst{\frac{\Delta g \times \Delta h}{\Delta x}}.\)\\
\pause
Se \(g\) e \(h\) sono derivabili, 
\(\dfrac{\Delta g}{\Delta x}\) e \(\dfrac{\Delta h}{\Delta x}\) sono numeri finiti,
perciò sia \(\Delta g\) sia \(\Delta h\) sono infinitesimi.
\pause

Quindi \(\dfrac{\Delta g}{\Delta x} \times \Delta h\) è infinitesimo e 
\(\pst{\dfrac{\Delta g \times \Delta h}{\Delta x}} =
  \pst{\dfrac{\Delta g}{\Delta x} \times \Delta h} = 0\)
  \spause
Avendo visto che \(\frac {\Delta F}{\Delta x}\) ha sempre la stessa parte 
standard per ogni infinitesimo \(\Delta x\ne0\), si può concludere che c’è
\(F’\) in \(x\) e che
\[
F'(x) = g' \times h + g \times h'.
\]
\end{frame}

\section{Derivata della composizione di funzioni}

\begin{comment}
\begin{frame} \frametitle{Derivare una funzione composta: cosa abbiamo già 
visto}
Sia \(f\) una funzione derivabile in \(c\). Allora:
\(f'(c+\Delta x)=f'(c)\Delta x +f(c)+ \epsilon \Delta x\),
per qualsiasi \(\Delta x\) infinitesimo e per \(\epsilon\), infinitesimo 
dipendente da \(\Delta x\).\\
\spause
Poiché l'equazione della tangente in \((c, f(c))\) è 
\(t(x)=f(c)\Delta x +f(c)\),  dal teorema si ricava che in 
\((c+\Delta x, f(c+\Delta x))\) l'incremento \(\Delta f=f(c+\Delta x)-f(c)\)
della funzione differisce dal corrispondente incremento 
\(\Delta t=t(c+\Delta x)-t(c)\) della tangente per 
\(\Delta f-\Delta t=\epsilon \Delta x\). \\
\spause
Perciò, considerando  \(f'(c) \Delta x\) in luogo 
\(\Delta f\) si trascurerà una quantità trascurabile anche rispetto a 
\(\Delta x\).
\end{frame}
\end{comment}

\begin{frame} \frametitle{Richiamiamo il teorema dell'incremento}
\textbf{Teorema dell'incremento}*.
Sia \(f\) una funzione derivabile in \(c\). Allora:
\(\bm{f(c+\Delta x)={\color{green!50!black}f'(c)\Delta x +f(c)}+ \epsilon \Delta x}\),
per qualsiasi \(\Delta x\) infinitesimo c'è un opportuno infinitesimo
\(\epsilon\), dipendente anche da \(\Delta x\).\\
\spause
Poiché l'equazione della tangente \(t\) alla funzione nel punto di
ascissa \(c\) è 
\({\color{green!50!black}t(x)=f'(c)\Delta x +f(c)}\),  dal teorema si ricava che l'incremento
\(\Delta f=f(c+\Delta x)-f(c)\) della funzione differisce dal corrispondente
incremento \(\Delta t=t(c+\Delta x)-t(c)\) della tangente per 
\(\bm{\Delta f-\Delta t=\epsilon \Delta x}\), che è un infinitesimo rispetto a
\(\Delta x\). \\
\spause
Perciò, considerando  \(f'(c) \Delta x\) in luogo di
\(\Delta f\) si trascurerà una quantità trascurabile anche rispetto a 
\(\Delta x\).\\
\vspace{.5cm}
{\footnotesize{(* vedi dia 14 nella presentazione precedente.)}}
\end{frame}

\begin{frame} \frametitle{Derivata della funzione composta}
Siano \(g\) e \(h\) due funzioni con \(g\) derivabile in \(c\) e \(h\) 
derivabile in \(g(c)\). Consideriamo la funzione composta \(F(x)=h(g(x))\).
Allora \(F\) è derivabile in \(c\) e \(F'(c)=h'(g(c))\cdot g'(c)\).

\spause
Chiamiamo: \(t=g(x)\), \(y=h(t)\) e anche \(y= F(x)\), così: 
\[
 {\color{red!50!black} F: x \overset {g}{\longrightarrow} 
 t \overset {h}{\longrightarrow} y }.
\]
% \spause
% Ecco una possibile rappresentazione grafica della composizione di 
% due funzioni, che commenteremo a voce.
\end{frame}
 

\begin{frame}\frametitle{Un primo tentativo (1)}
Svilupperemo un primo tentativo di dimostrazione e ci soffermeremo sulle
difficoltà che comporta.
\spause
Si sta cercando il rapporto incrementale di \(F\) per esempio in un punto
di ascissa  \(x=c\).\\
\(\frac{\Delta F}{\Delta x} = \frac{F(c+\Delta x)-F(c)}{\Delta x} = 
\frac{h(g(c+\Delta x))-h(g(c))}{\Delta x}\) \\
\pause
%Poiché \(g\) e \(h\) sono per ipotesi derivabili, ci sono 
Si ricordi che le notazioni\quad
\(\frac{\Delta g}{\Delta x}\) e  \(\frac{\Delta h}{\Delta t}\) 
\quad indicano rispettivamente:

\begin{center}\(\frac{\Delta g}{\Delta x} = \frac{g(c+\Delta x)-g(c)}{\Delta x}\)
\quad e \quad
\(\frac{\Delta h}{\Delta t} = \frac{h(t+\Delta t)-h(t)}{\Delta t}\).\end{center} 
\spause
Avendo definito \(t=g(x)\), saremmo portati a scrivere:\\
\(\Delta t=\Delta g=g(c+\Delta x)-g(c)\)\quad e\quad
\(t+\Delta t\ = g(c+\Delta x)\),\\
\vspace{3mm}
sicché \(\frac{\Delta h}{\Delta t}\) si potrebbe pensarlo così:  
\(\frac{\Delta h}{\Delta t}=\frac{h(g(c+\Delta x))-h(g(c))}
    {g(c+\Delta x)-g(c)}\). \\
\spause
Il numeratore corrisponde a \(\Delta F\), il denominatore 
{\color{red!50!black}richiama} \(\Delta g\).
\end{frame}

\begin{frame}\frametitle{Un primo tentativo (2)}

{\color{red!50!black}Tentazione irresistibile:
\begin{align*}
\frac{\Delta h}{\Delta t} \cdot \frac{\Delta g}{\Delta x}&=
\frac{h(g(c+\Delta x))-h(g(c))}{\cancel{g(c+\Delta x)-g(c)}}\cdot
\frac{\cancel{g(c+\Delta x)-g(c)}}{\Delta x}=\\
&=\frac{h(g(c+\Delta x))-h(g(c))}{\Delta x}=\frac{\Delta F}{\Delta x}.
\end{align*}
}
Così si concluderebbe che \(\frac{\Delta F}{\Delta x} = \frac{\Delta h}{\Delta t} 
\cdot \frac{\Delta g}{\Delta x}\) e, 
poiché le parti standard di questi fattori ci sono e sono sempre le stesse, 
anche la parte standard del prodotto ci sarà e sarà sempre la stessa. Si 
concluderebbe anche che \(F’ = h’\cdot g’\). \pause Ma...
\spause
Ma il denominatore \(\Delta t\) indica cose diverse dal numeratore 
\(\Delta g=g(c+\Delta x)-g(c)\), che potrebbe anche annullarsi
e semplificarli è sbagliato, sebbene la notazione coincida.
\end{frame}

\begin{frame}\frametitle{Un primo tentativo (3)}
La ragione della diversità dipende dal significato dei simboli: in
\[\frac{\Delta h}{\Delta t} \cdot \frac{\Delta g}{\Delta x}\]
% il denominatore \(\Delta t=g(c+\Delta x)-g(c)\)
% deve essere una variabile indipendente della funzione \(h\): 
\(\Delta t\) 
indica tutti i possibili incrementi infinitesimi diversi da zero della 
variabile \(t\).
\spause

Il numeratore \(\Delta g=g(c+\Delta x)-g(c)\) indica la variazione
della funzione \(g\) relativa a \(\Delta x\), incremento di \(x\), variabile 
indipendente della funzione \(g\). 
\end{frame}

\begin{frame}\frametitle{Un primo tentativo (4): una via d'uscita}
Però, se si potesse mostrare che, sotto opportune ipotesi, in corrispondenza
di tutti gli infinitesimi non nulli \(\Delta x\) la funzione \(g\) fa ottenere
soltanto infinitesimi non nulli \(\Delta t\), avremmo superato l'ostacolo.\\
\pause 

Si aggiunga allora l’ulteriore ipotesi \(g’(c) \ne 0\) e sia \(r\) la retta
tangente alla funzione nel punto di ascissa \(c\). Le conseguenze:\\
\pause

{\color{green!50!black}\begin{center}\(r\) non è orizzontale,\end{center}}
perché il suo coefficiente angolare è \(g’(c)\)
che ora è diverso da \(0\).
\pause
Pertanto, per ogni infinitesimo \(\Delta x \ne 0\) 
si ha:
{\color{green!50!black}\begin{center}\(r(c+\Delta x) \ne r(c)\)\end{center}}
e quindi:
\pause
{\color{green!50!black}\begin{center}\(\Delta r = r(c+\Delta x) - r(c)\ne 0\)
\end{center}}
e, per ogni infinitesimo \(\Delta x \ne 0\):
\pause
{\color{green!50!black}\begin{center}\(\Delta g\ne 0\).\end{center}}
\end{frame}

\begin{frame}\frametitle{Un primo tentativo (5): una via d'uscita}
% Ciò comporta che, per ogni infinitesimo \(\Delta x \ne 0\), anche 
% l’infinitesimo \(\Delta g\) sia diverso da 0. 
Infatti, dal teorema dell’incremento, abbiamo che, per ogni infinitesimo 
\(\Delta x \ne 0\),
{\color{green!50!black}\begin{center}\(\Delta g - \Delta r= \epsilon \Delta x\)
\end{center}}
per un opportuno infinitesimo \(\epsilon\), sicché, pur spostandosi di 
\(\Delta x\), % da \(c\), 
\pause 
{\color{green!50!black}\begin{center}\(\Delta g \ne 0\) è troppo vicino a \(\Delta r\)
per poter essere \(0\).\end{center}}
\pause
Così, dall’ipotesi \(g’(c)\) segue che, per ogni infinitesimo \(\Delta x\ne 0\),
{\color{green!50!black}\begin{center}\(g\) fa ottenere soltanto infinitesimi non 
nulli \(\Delta t\)\end{center}}

e, con l’ipotesi aggiunta, si è superata l’esigenza di distinguere \(\Delta g\) da 
\(\Delta t\).\\

Così si ottiene di giustificare la regola della derivata della 
funzione composta nel caso selezionato.

% 
% In certi casi, questo si può, grazie al teorema dell’incremento. 
% Sia \(r\) la retta tangente in \((c, g(c)):\)\\
% \[ \Delta r= r(c+\Delta x)- r(c) =g'(c)\Delta x.\]
% \[
%  \Delta g - \Delta r= \epsilon \Delta x.
%  \]
% per un opportuno \(\epsilon\).\\
% Supponiamo \(g'(c)\ne 0\). Allora, \( \forall \Delta x\ne 0\), si ha
% \(\Delta r\ne 0\), quindi \(r\) non è una retta orizzontale e 
% \(r(c+\Delta x)\ne g(c)\).\\
% \pause
% Allora anche \(g(c+\Delta x)\ne g(c)\), infatti differisce da \(\Delta r\)
% solo per \(\epsilon \times \Delta x\).
\end{frame}

\begin{frame}[shrink=3]
\frametitle{Un primo tentativo (6): una via d'uscita}
Resta da dimostrare che anche quando \(g’(c)=0\) questa regola è giustificata;
cioè bisogna dimostrare che:
\begin{center}se \(g’(c)=0\) allora \(F'(c)=0\).\end{center}
\pause

La dimostrazione sarebbe semplice se l’annullarsi di \(g’\)
comportasse che, per ogni infinitesimo \(\Delta x \ne 0\), fosse 
\(g(c+\Delta x) = g(c)\), così la funzione \(g\) sarebbe costante
nella monade di \(c\).\\
\pause
Infatti,
\begin{itemize}[<+->]
\item applicando la funzione \(h\) sempre allo stesso valore si
otterrebbe sempre lo stesso valore e anche la funzione \(F\) sarebbe costante
almeno nella monade di \(c\)
\item e la sua derivata sarebbe \(0\), come si è già visto, giustificando così 
la regola anche in questo caso, particolarissimo.
\item Ma non è detto che per ogni infinitesimo \(\Delta x \ne 0\) sia 
\(g(c+\Delta x) = g(x)\), pur essendo \(g’(x) = 0\), e bisogna superare anche
questo ulteriore ostacolo.
\end{itemize}
\end{frame}

\begin{frame}\frametitle{La via d'uscita}
Ecco allora che viene l’idea di ricorrere alla tangente di \(g\) che,
avendo coefficiente angolare \(0\), è una costante.
\spause
Poi si aggiusterà il tiro grazie all’infinita vicinanza tra tangente e
funzione, anche rispetto a \(\Delta x\), certificata dal teorema
dell’incremento.
\spause
Si torna quindi a focalizzarsi sul teorema dell’incremento. La
regola della derivata della composizione di funzioni si potrebbe
più facilmente dimostrare attraverso la composizione delle
tangenti e con l’aiuto del teorema dell’incremento.
\end{frame}


\begin{frame}\frametitle{Se \(g(x)\) e \(h(t)\) fossero rette}%
La composizione \(y=r_2(r_1(x))\) di due funzioni lineari:
\[t=r_1(x)={\color{green!50!black}a}\cdot x+c\quad \text{e}
\quad y=r_2(t)={\color{green!50!black}b}\cdot t+d\]
dà la funzione lineare
\[y={\color{green!50!black}b}\cdot ({\color{green!50!black}a}\cdot x+c)+d =
{\color{green!50!black}ba}\cdot x+ bc+d,\]
il cui coefficiente angolare \({\color{green!50!black}ba}\) è il prodotto 
dei coefficienti angolari delle rette componenti.

\[\Delta r_1 ={\color{green!50!black}a}\cdot\Delta t \quad
 \Delta r_2 = {\color{green!50!black}b}\cdot\Delta x \quad
 \Delta y = {\color{green!50!black}ba}\cdot\Delta x. 
\]
\pause
Ovvero, la pendenza della funzione composta di funzioni lineari è il prodotto
delle pendenze delle componenti.
\end{frame}

\begin{frame}[shrink=3]
\frametitle{La dimostrazione corretta}
Grazie al teorema dell'incremento la regola della derivata della funzione 
composta si dimostra rapidamente, senza distinguere i casi.
 \begin{itemize}
\item Anzitutto, poiché esiste \(g'(c)\), \(\Delta g(c, \Delta x)\)
è infinitesimo per ogni infinitesimo \(\Delta x \).
\item Poi, poiché esiste \(h'(t_0)\), con \(t_0=g(c)\), dal teorema 
dell'incremento si deduce che per ogni infinitesimo \(\Delta t\)
c'è un infinitesimo \(\epsilon\) tale che
\(\Delta h(t_0, \Delta t) = h'(t_0)\cdot\Delta t+
\epsilon\cdot\Delta t.\)
\end{itemize}
\pause
Così, dividendo per qualsiasi infinitesimo non nullo \(\Delta x\),
%{\footnotesize
 \begin{align*}
 &\pst{\frac{\Delta F(c, \Delta x)}{\Delta x}}
 =\pst{\frac{\Delta h(t_0, \Delta t)}{\Delta x}}=\\
 &=\pst{\frac{h'(t_0)\cdot\Delta t+\epsilon\cdot\Delta t}{\Delta x}}=
 \pst{\frac{h'(t_0)\cdot\Delta t}{\Delta x}}+
   \pst{\frac{\epsilon\cdot\Delta t}{\Delta x}}=\\
 &= h'(t_0)\cdot \pst{\frac{\Delta t}{\Delta x}}+ 
   \pst{\epsilon}\cdot\pst{\frac{\Delta t}{\Delta x}}=
 h'(t_0)\cdot g'(c).
\end{align*}
\end{frame}


\begin{frame} \frametitle{Osserviamo la funzione composta}
\begin{columns} [T]
\begin{column}{.75\textwidth}
\derivatacomposta 
\end{column}
\begin{column}{.25\textwidth}
{\fontsize{10pt}{7.2}\selectfont
Funzione\\
{\color{violet!50!black} \[F(x)=f(g(x))\]} 

Rappresentiamo\\ {\color{red!50!black} \(t=g(x)\)} \vspace{.5em}

\only <2->{Ribaltiamo} \vspace{.2em}
\only <3->{e ruotiamo il piano \vspace{.5em}}

\only <7->{
Rappresentiamo:\\ {\color{blue!50!black} \(y=h(t)\)} \vspace{.5em}}

\only <8->{
Rappresentiamo:\\ {\color{violet!50!black} \(y=F(x)\)} \vspace{.5em}}

\only <9->{
Usiamo microscopi con ingrandimento infinito \(\dfrac{1}{\Delta x}\)}
}
 \end{column}
\end{columns}
\end{frame}


\begin{comment}
\begin{frame}\frametitle{Sulla notazione}
 La regola della composizione viene espressa variamente e più la 
 notazione è sintetica e più facilmente si perdono i dettagli.
 
 Noi abbiamo ottenuto:  
 \( F'(x)=h'(t)\times g'(x)\quad \text{ dove } t=g(x).
 \)\\
\vspace{3mm}
Nella notazione che utilizza i simboli di variabile al posto dei simboli di 
funzione spesso si scrive \(\frac{dy}{dx}=\frac{dy}{dt}\times\frac{dt}{dx}\), 
e si può essere indotti a credere lecita la semplificazione di \(dt\).

Meglio sarebbe esplicitare le variabili di derivazione, per esempio:
\[
 \tonda{ \frac{dy}{dx}}_x=\tonda{\frac{dy}{dt}}_{g(x)}\tonda{{\frac{dt}{dx}}}_x.
\]

Si tratta di un abuso di notazione che può essere accettato solo in quanto 
si è già dimostrato il teorema, per esempio nel modo appena esposto.
\end{frame}




\section{Derivata della funzione inversa}

\begin{frame}[shrink=5]
\frametitle{Derivata della funzione inversa}
 Questa regola ottiene facilmente da quanto appena esposto.
 
Si sa che le funzioni invertibili sono biettive e che la composizione di una
funzione con la sua inversa, se questa esiste nei punti che si corrispondono,
dà la funzione identica e che la derivata della funzione identica c'è e vale 1.\\
\spause
Sia \(f^{-1}\) l'inversa di \(f\), siano entrambe funzioni derivabili, con derivata
non nulla. Allora  \((f^{-1}(f(x)))'=\frac{1}{f'(x)}\). \\

Applichiamo la regola della derivata della funzione composta a \(f^{-1}(f(x))\):
 {\footnotesize
  \[
  \pst{\frac{\Delta f^{-1}(f(x))}{\Delta x}}=\pst{\frac{\Delta f^{-1}(y)}
  {\Delta y}\times  \frac{\Delta f(x)}{\Delta x}}= \pst{\frac{\Delta x}
  {\Delta y}}\times \pst{\frac{\Delta y}{\Delta x}}=1.
 \] 
 }
 Per ipotesi entrambi i fattori ci sono e, se il loro prodotto è \(1\), sono non
 nulli e reciproci.
 
\end{frame}


\begin{frame}[shrink=3]
\frametitle{\(f\) e \(f^{-1}\) nel Piano cartesiano}

\begin{columns} [T]
\begin{column}{.5\textwidth}
\diffinversa 
\end{column}
\begin{column}{.5\textwidth}
 I punti \((x, f^{-1}(x))\) sulla curva \(y=f^{-1}(x)\)  hanno per corrispondenti
 nella simmetria i punti \((f^{-1}(f(x)), x)\) sulla curva di \(f\).
 
 In pratica le coordinate dei punti corrispondenti sulle due curve hanno
 i valori scambiati.\\
 Lo stesso avviene confrontando gli incrementi che si corrispondono sulle due curve.
 \end{column}
\end{columns}

La rappresentazione può risultare utile didatticamente. Vale anche per gli incrementi
non infinitesimi.
\end{frame}

\end{comment}

%\section{Conclusioni}

\begin{frame}\frametitle{Riassunto}
Sono state esposte in dettaglio due diverse giustificazioni non standard di due note 
regole di derivazione: 
\begin{enumerate} %[<+->]
\item la regola della derivata del prodotto di due funzioni,

\item la regola della derivata della composizione di due funzioni

\end{enumerate}
Nella seconda, in particolare, l'uso del teorema dell'incremento si rivela uno
strumento essenziale per giungere alla dimostrazione.
\end{frame}



\begin{frame}\frametitle{Il tuo contributo}
Si affrontino alcune delle seguenti questioni.
\begin{enumerate}
\item Si concluda la dimostrazione della regola della somma, introdotta nella
slide 1.
\item Si dimostri la regola della derivata del reciproco di una funzione.
% \item Si dimostri la regola della derivata del quoziente fra due funzioni.
\item Suggerite una rappresentazione grafica migliore della regola della funzione
composta.
\item Che strano: non si è ancora parlato di limiti e sappiamo già derivare...
\item Veramente mi sarei aspettato che qualcuno garantisse la continuità delle 
funzioni di cui si parla!
\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Se tutto questo sembra promettente\dots}
Possiamo:
\begin{itemize}
\item Fare il punto sulle funzioni a variabile iperreale, che sembrano comportarsi
esattamente come al solito;
\item cominciare ad occuparci della continuità e dei teoremi sulle funzioni continue.
\end{itemize}
\end{frame}

      %--------------------------------------------------------------%

