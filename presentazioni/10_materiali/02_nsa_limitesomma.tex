%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        Corso Analisi Non Standard - Verona 2020
%
%                      Ruggero Ferro
%
%             Limite della somma di duefunzioni
%
%------------------------------
% Compilazione:
% pdflatex --shell-escape 01_nsa_limiti.tex
%------------------------------
%
% This work may be distributed and/or modified under the
% conditions of the Creative Commons BY-SA
%
% Authors: Ruggero Ferro
%
% The Current Maintainer of this work is 
% Daniele Zambelli - daniele.zambelli@gmail.com
%
% Copyright 2020-24 Bruno Stecca
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%========================
% Definizione delle directory
%========================
\newcommand{\depdir}{../../}
\newcommand{\lbr}{\depdir lbr/}

%========================
% Tipo di documento
%========================
\documentclass[a4paper, 12pt]{report}
\usepackage[margin=20mm]{geometry}

%========================
% Lettura preambolo e definizioni
%========================
\input{\lbr preambolo_guide}
\input{\lbr definizioni_guide}
\graphicspath{ {./img/} }

\begin{document}
\begin{center}
\Large{
\textbf{Confronto tra la trattazione non standard e standard del teorema sul 
limite di una somma}}

\emph{Ruggero Ferro}
\end{center}

\subsection*{Trattazione non standard}

\emph{Nozione di limite non standard}: il numero reale \(L\) è il 
\(\displaystyle \lim_{x \to c}f(x)\), con 
\(c\) numero reale, se per ogni iperreale \(x\) diverso da \(c\) ma 
infinitamente vicino a \(c\) risulta che \(f(x)\) è 
infinitamente vicino a \(L\). 
Detto altrimenti, per ogni \(x\) iperreale se 
\(x \neq c \stext{ e } x \approx c \stext{ allora } f(x)\approx L\); 
ovvero, per ogni \(x\) iperreale, se 
\(x \neq c \stext{ e } \pst{x}=c \stext{ allora } \pst{f(x)} = L\), 
cioè \(\forall x\tonda{\tonda{x \neq c \stext{ e } x \approx c)} \srarrow 
f(x) \approx L}\).

\emph{Poiché la successiva dimostrazione sarà un’applicazione di questo 
risultato lo si include affinché il confronto sia completo.}
Parte standard della somma \(x+y\) è la somma delle parti standard di \(x\) 
e di \(y\), se ci sono. 
Infatti, poiché la parte standard di un numero differisce da quello per 
un infinitesimo 
(nel nostro caso \(x-\pst{x} = \alpha\) e \(y-\pst{y} = \beta\)) 
e siccome la somma di infinitesimi è un infinitesimo 
(\(\alpha + \beta = \gamma\)), allora: 
\(x+y-\tonda{st(x)+st(y)}=\gamma\) e \(\pst{x+y}= \pst{x} + \pst{y}\) 
Come volevasi.

\vspace{1em}
\textsc{Teorema.} Il limite della somma è la somma dei limiti, se questi ci 
sono.
\textsc{Dimostrazione.}
Sia \(f(x)=g(x)+h(x)\) e siano \(\displaystyle a = \lim_{x \to c}g(x)\) e
\(\displaystyle b=\lim_{x \to c}h(x)\)
Si vuole mostrare che 
\(\displaystyle \lim_{x \to c}f(x) = a + b\). 
Sia \(x\) un qualsiasi iperreale 
diverso da \(c\) e infinitamente vicino a \(c\).
Dalle ipotesi sappiamo che allora: \(\pst{g(x) = a}\) e \(\pst{h(x) = b}\), 
sicché
\(\pst{f(x)} = \pst{g(x)+h(x)} =\) (per il comportamento della parte standard) 
\(\pst{g(x)} + \pst{h(x)} = a + b\).

\subsection*{Trattazione standard}
\emph{Nozione di limite standard}
\(\displaystyle L = \lim_{x \to c}f(x)\) se per ogni numero reale positivo 
\(\epsilon\) c'è un numero reale positivo \(\delta\) tale che per ogni reale 
\(x\) diverso da \(c\) ma più vicino a \(c\) di \(\delta\) si ha che 
\(\abs{f(x) - L} < \epsilon\). 
Detto altrimenti, per ogni numero reale 
positivo \(\epsilon\) c'è un numero reale positivo \(\delta\) tale che 
per ogni reale \(x\), 
\(x \neq c\) e \(\abs{x-c} < \delta\) implica 
\(\abs{f(x) - L} < \epsilon\), 
ovvero: \\
\(\forall \epsilon \quad \exists \delta \quad \forall x
\tonda{\tonda{\epsilon > 0 \stext{ e } \delta > 0 \stext{ e } 
x \neq c \sand \abs{x - c} < \delta} \srarrow 
\abs{f(x) - L} < \epsilon}\)

\vspace{1em}
\textsc{Teorema.} Il limite della somma è la somma dei limiti, se questi ci 
sono.
\textsc{Dimostrazione.}
Sia \(f(x)=g(x)+h(x)\) e siano 
\(\displaystyle a = \lim_{x \to c}g(x)\) e
\(\displaystyle b=\lim_{x \to c}h(x)\)
Si vuole mostrare che 
\(\displaystyle \lim_{x \to c}f(x) = a + b\). 
Allo scopo, sia \(\epsilon\) un arbitrario numero reale positivo, si 
vuole trovare un numero reale positivo \(\delta\) tale che per ogni \(x\) tale 
che 
\(x \neq c\) e \(\abs{x-c} < \delta\)
risulti \(\abs{f(x) - (a+b)} < \epsilon\). 
Dalle ipotesi ci sono \(\delta_1\) tale che per ogni \(x\) tale che 
\(x \neq c\) e \(\abs{x-c} < \delta_1\) risulti 
\(\abs{g(x) - a} < \frac{\epsilon}{2}\), 
e \(\delta_2\) tale che per ogni \(x\) tale che 
\(x \neq c\) e \(\abs{x-c} < \delta_2\) risulti 
\(\abs{h(x) - b} < \frac{\epsilon}{2}\). 
Sia \(\underline{\delta} = min(\delta_1,~\delta_2)\), allora per 
ogni \(x\) tale che \(x \neq c \stext{ e } 
\abs{x-c} < \underline{\delta}\) 
valgono sia 
\(\abs{g(x) - a} < \frac{\epsilon}{2}\) che 
\(\abs{h(x) - b} < \frac{\epsilon}{2}\). 
Sommando i termini a sinistra delle 
disuguaglianze e quelli a destra, si ottiene: \\
\(\abs{f(x) - (a+b)} = \abs{g(x)+h(x) - (a+b)} < 
\abs{g(x) - a} + \abs{h(x) - b} < 
\frac{\epsilon}{2} + \frac{\epsilon}{2} = \epsilon\), 
ancora per ogni \(x\) tale che 
\(x \neq c\) e \(\abs{x-c} < \underline{\delta}\), 
sicché \(\underline{\delta}\) è la quantità 
cercata corrispondente al numero reale \(\epsilon\), 
qualsiasi questo sia, e si può concludere che
\(\displaystyle \lim_{x \to c}f(x) = a+b\).

\end{document}
