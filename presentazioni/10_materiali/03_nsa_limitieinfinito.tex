%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        Corso Analisi Non Standard - Verona 2020
%
%                      Daniele Zambelli
%
%                 Limiti e problemi didattici
%
%------------------------------
% Compilazione:
% pdflatex --shell-escape 03_nsa_limitieinfinito.tex
%------------------------------
%
% This work may be distributed and/or modified under the
% conditions of the Creative Commons BY-SA
%
% Authors: Ruggero Ferro
%
% The Current Maintainer of this work is 
% Daniele Zambelli - daniele.zambelli@gmail.com
%
% Copyright 2020-24 Bruno Stecca
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%========================
% Definizione delle directory
%========================
\newcommand{\depdir}{../../}
\newcommand{\lbr}{\depdir lbr/}

%========================
% Tipo di documento
%========================
\documentclass[a4paper, 12pt]{report}
\usepackage[margin=20mm]{geometry}

%========================
% Lettura preambolo e definizioni
%========================
\input{\lbr preambolo_guide}
\input{\lbr definizioni_guide}
\graphicspath{ {./img/} }

\begin{document}
\begin{center}
\Large{
\textbf{Limiti e problemi didattici}}

\emph{Daniele Zambelli}
\end{center}

\subsection*{Lo spunto di partenza da una mail}
``Riporto nuovamente il problema che sollevai qualche incontro fa.
Consideriamo la funzione <<\(1/x\)>>  con <<\(x \in \Rz\)>>.
Il limite per <<\(x \to 0\)>> di <<\(1/x\)>> non esiste né per l'analisi 
standard, né per l'analisi non standard. 
Il limite destro (+infinito) e quello sinistro 
(-infinito) sono diversi; stando al Keisler i due limiti non sono uguali e 
quindi il limite non esiste. Ma, a parte ciò, essi non esistono, sempre 
secondo il Keisler, semplicemente perché le loro parti standard non esistono 
(pag.122).
Se consideriamo il limite di <<\(1/x^2\)>> per <<\(x \to 0\)>>, sempre con 
<<\(x \in \Rz\)>>, in analisi standard il limite esiste ed è <<+infinito>> 
(inteso come processo), mentre non esiste come numero in analisi non standard 
(pag. 122). 
% 
% In base a 
% quanto scrive Keisler, non esiste la parte standard di un infinito (pag. 239 
% prime righe) e tuttavia lui stesso scrive che tale limite è <<+infinito>>. 
[\dots]''

% Keisler, alla scrittura <<+infinito>> dà il senso di processo (lo stesso 
% senso che troviamo in analisi standard), non di numero (pag. 239). 
% Quindi mi pare che gli infiniti, per Keisler, esistono sia come numeri e 
% sia come processi. 
% È giusto? 
% Comunque, quanto riportato alle pagine 122, 237 e 239 del Keisler riguardo 
% a questo problema sarebbe da chiarire meglio.
% Vi chiedo lumi.

\subsection*{Un passo indietro}

Allarghiamo un po' lo sguardo e inseriamo il problema in un contesto.
\begin{enumerate} [noitemsep]
\item 
I limiti sono stati inventati per poter calcolare derivate e integrali senza 
utilizzare gli \emph{infinitesimi} e gli \emph{infiniti}.
\item 
Nel calcolo di derivate e integrali non usiamo limiti infiniti, 
questi sono invece utili nello studio delle funzioni quando
si allontanano infinitamente dall'origine degli assi.
\item 
Molti argomenti di analisi possono essere trattati senza considerare i 
limiti infiniti. In certi testi di analisi questi non sono neppure presentati 
o sono trattati dopo i capitoli sull'integrazione e la derivazione.
\item 
Nell'analisi standard <<\(\infty\)>>, di solito, indica il comportamento 
di una funzione e è un simbolo che va usato con una certa attenzione per 
evitare fraintendimenti.
\item 
I limiti danno come risultato il comportamento di una funzione vicino ad un 
punto e a volte questo comportamento può essere indicato da un numero a volte 
da un altro simbolo(\(-\infty \stext{ o} +\infty\)).
\end{enumerate}

\subsection*{Limite finito al finito}

Nell'analisi non standard diremo che 
il limite di una funzione \(f(x)\) per \(x \to c\) è \(L\) se e solo se 
\(L\) è un numero reale infinitamente vicino a \(f(c + \epsilon)\) qualunque 
sia l'infinitesimo non nullo \(\epsilon\). 
\[\lim_{x \to c} f(x) = L \stext{ se } \forall x \approx c \stext{ e } 
x \neq c, f(x) \approx L\]
Dato che il numero reale infinitamente vicino ad un iperreale è proprio il 
risultato della parte standard, possiamo scrivere:
\[\lim_{x \to c} f(x) = \pst{f(c + \epsilon)} \]

\vspace{-1.em}
\begin{center}
se la parte standard esiste ed è sempre la stessa per ogni 
infinitesimo \(\epsilon \neq 0\).
\end{center}

\emph{Osservazioni}
\begin{itemize}
\item 
La definizione di limite richiede soltanto il concetto di 
\emph{infinitamente vicino}, la sua formulazione con la parte standard 
fornisce un metodo per \emph{calcolare} il limite.
\item 
Questa definizione va bene per i limiti finiti, ma non copre i limiti 
infiniti, infatti se \(f(c + \epsilon)\) è un numero infinito 
\subitem non esiste nessun numero reale \(L \approx f(c + \epsilon)\);
\subitem non possiamo calcolare la parte standard di \(f(c + \epsilon)\).
\end{itemize}

\subsection*{Limite infinito al finito}

Possiamo ampliare la definizione non standard di limite in modo da includere 
anche i limiti infiniti. 
Usando i simboli di infinito sempre con lo stesso significato che diamo loro 
nell'analisi standard, possiamo ampliare la definizione di limite aggiungendo 
il limite infinito:
\[\lim_{x \to c} f(x) = -\infty \quad 
\stext{ se } f(c+\epsilon) 
\stext{ è un infinito negativo per ogni infinitesimo } \epsilon \neq 0\]
\[\lim_{x \to c} f(x) = +\infty \quad 
\stext{ se } f(c+\epsilon) 
\stext{ è un infinito positivo per ogni infinitesimo } \epsilon \neq 0\]

\vspace{.5em}
Quindi, possiamo prima calcolare il valore (iperreale) di 
\(f(c+\epsilon)\) e poi:

% \renewcommand{\labelitemii}{$\diamond$}
% \renewcommand{\labelitemii}{$\triangleright$}
% \renewcommand{\labelitemii}{$\RHD$}
% \renewcommand{\labelitemi}{\(\triangleright\)}
\renewcommand{\labelitemii}{\(\circ\)}
% \renewcommand{\labelitemii}{\(\bullet\)}

\begin{itemize}
\item 
se \(f(c+\epsilon)\) è \emph{finito} il limite sarà la sua parte standard se 
questa è indipendente da \(\epsilon\);
\item 
se \(f(c+\epsilon)\) è \emph{infinito} il limite sarà: 
  \begin{itemize} [nosep]
  \item \(+\infty\) se per qualunque infinitesimo 
    \(\epsilon \neq 0\), \(f(x + \epsilon)\) è un infinito positivo; 
  \item \(-\infty\) se per qualunque infinitesimo 
    \(\epsilon \neq 0\), \(f(x + \epsilon)\) è un infinito negativo. 
  \end{itemize}
\end{itemize}

Possiamo iniziare calcolando il valore della funzione in un punto 
infinitamente vicino al punto in cui si vuole calcolare il limite.
Poi, a seconda del valore ottenuto, si applicherà la prima o la seconda 
definizione.

Vediamo un paio di esempi.

\vspace{1em}
\textbf{Primo esempio} 
Studia il comportamento della funzione 
\(f(x) = \dfrac{x^2-4}{x^2+x-6}\)
vicino a \(2\). 
\begin{align*}
f(2 + \epsilon) \stackrel{1}{=}
\frac
  {\tonda{2+\epsilon}^2-4}
  {\tonda{2+\epsilon}^2+\tonda{2+\epsilon}-6} \stackrel{2}{=}
\frac
  {\cancel{+4}+4\epsilon+\epsilon^2~\cancel{-4}}
  {\cancel{+4}+4\epsilon+\epsilon^2~\cancel{+2}+\epsilon~\cancel{-6}}
\stackrel{3}{=}
\frac
  {\cancel{\epsilon} \tonda{4+\epsilon}}
  {\cancel{\epsilon} \tonda{5+\epsilon}}
\end{align*}
Dato che il valore ottenuto è finito 
(il quoziente di due finiti non infinitesimi) 
applicheremo la definizione di limite finito:
\begin{align*}
\lim_{x \to 2} \frac{x^2-4}{x^2+x-6} \stackrel{4}{=} 
\pst{\frac{4+\epsilon} {5+\epsilon}} \stackrel{5}{=}
\frac{\pst{+4+\epsilon}}{\pst{+5+\epsilon}} \stackrel{6}{=} \frac{4}{5}
\end{align*}
Dato che il valore ottenuto è indipendente da \(\epsilon\), 
\(\dfrac{4}{5}\) è il limite cercato.
\begin{enumerate} [nosep]
\item calcoliamo \(f(2+\epsilon)\);
\item otteniamo il rapporto tra due infinitesimi 
dato che tutti i termini non infinitesimi si annullano;
\item possiamo raccogliere e semplificare \(\epsilon\);
\item applichiamo la definizione di limite finito;
\item la parte standard della frazione è uguale al rapporto delle parti 
standard del numeratore e del denominatore dato che il denominatore non è 
infinitesimo;
\item la parte standard di un reale più un infinitesimo è il reale stesso
e non dipende dall'infinitesimo \(\epsilon\).
\end{enumerate}

\vspace{1em}
\textbf{Secondo esempio} 
Studia il comportamento della funzione 
\(f(x) = \dfrac{-2x+6}{x-4}\)
vicino a \(4\). 
\[f(4 + \epsilon) = 
  \frac{-8 - 2 \epsilon + 6}{4 - \epsilon - 4} =
  \frac{-2 - 2 \epsilon}{\epsilon} \]
Dato che un numero finito fratto un infinitesimo è un infinito, 
dovremo usare la definizione di limite infinito.
Ma, poiché al variare di \(\epsilon\) 
l'infinito che si ottiene può avere segno diverso,
% il valore ottenuto non è sempre positivo e non è neppure sempre negativo, 
non esiste il limite.

Qualcosa comunque possiamo ugualmente dire se ci accontentiamo dei limiti
parziali. 

Dato che l'infinito che otteniamo ha segno opposto al segno di \(\epsilon\), 
per valori di \(x\) infinitamente vicini a \(4\) e minori di \(4\) 
il valore della funzione (iperreale) è sempre un infinito positivo 
mentre per valori di \(x\) infinitamente vicini a \(4\) e maggiori di \(4\) 
la funzione (iperreale) ha sempre un valore infinito negativo. 
Quindi:
\[\lim_{x \to 4^-} \frac{-2x+6}{x-4} = +\infty \qquad 
\lim_{x \to 4^+} \frac{-2x+6}{x-4} = -\infty\]

\subsection*{Limite per \(x\) che tende all'infinito}

Finora abbiamo visto limiti per x che tende ad un valore finito, ma è 
interessante anche studiare i limiti 
quando \(x\) è un \emph{qualsiasi} valore infinito negativo  o 
quando x è un qualsiasi valore infinito positivo:
% \(x \to -\infty \stext{ o } x \to +\infty\):
\[\lim_{x \to -\infty} f(x) \qquad 
\lim_{x \to +\infty} f(x)\]
In questo caso studieremo il valore della funzione (iperreale) in ogni 
punto infinito negativo (positivo):
\[f(M)\]
poi a seconda del risultato ottenuto, applicheremo una o l'altra delle 
definizioni viste prima:
\begin{itemize}
\item 
Se \(f(M)\) è un valore finito:
\[\lim_{x \to -\infty} f(x) = \pst{f(M)}  \qquad
  \lim_{x \to +\infty} f(x) = \pst{f(M)}\]
\begin{center}
se la parte standard esiste ed è sempre la stessa rispettivamente \\
per ogni 
infinito negativo \(M\) o per ogni infinito positivo \(M\).
\end{center}
\item 
Se \(f(M)\) è un valore infinito:
\vspace{-1em}
\begin{align*}
\lim_{x \to -\infty} f(x) = -\infty \quad 
\stext{ se } f(M) 
\stext{ è un infinito negativo per ogni infinito } M < 0\\
\lim_{x \to -\infty} f(x) = +\infty \quad 
\stext{ se } f(M) 
\stext{ è un infinito positivo~ per ogni infinito } M < 0\\
\lim_{x \to +\infty} f(x) = -\infty \quad 
\stext{ se } f(M) 
\stext{ è un infinito negativo per ogni infinito } M > 0\\
\lim_{x \to +\infty} f(x) = +\infty \quad 
\stext{ se } f(M) 
\stext{ è un infinito positivo~ per ogni infinito } M > 0\\
\end{align*}
\end{itemize}

\textbf{Terzo esempio, primo modo}\\
Studia il comportamento della funzione 
\(f(x) = \dfrac{3x^2-3x+7}{5x^2-6}\)
per \(x \to +\infty\). 
\[f(M) \stackrel{1}{=} \frac{3M^2-3M+7}{5M^2-6} \stackrel{2}{=} 
  \frac{\cancel{M^2}\tonda{3-\frac{3}{M}+\frac{7}{M^2}}}
       {\cancel{M^2}\tonda{5-\frac{6}{M^2}}} 
  \]
Tenendo presente che:
\begin{itemize} [noitemsep]
\item il quoziente tra un finito e un qualsiasi infinito è un infinitesimo,
\item la somma tra un non infinitesimo e un infinitesimo è un non 
infinitesimo, 
\item il rapporto tra finiti non infinitesimi è un finito,
\end{itemize}
\(f(M)\) è quindi un finito, qualunque sia il valore infinito positivo di 
\(M\), perciò:
\[\lim_{x \to +\infty} \frac{3x^2-3x+7}{5x^2-6} \stackrel{3}{=} 
\pst{f(M)} \stackrel{4}{=} \frac{\pst{3-\frac{3}{M}+\frac{7}{M^2}}}
                                {\pst{5-\frac{6}{M^2}}} \stackrel{5}{=}
\frac{3}{5}\]
\begin{enumerate} [nosep]
\item Calcoliamo il valore della funzione in un generico iperreale infinito 
positivo;
\item raccogliamo \(M^2\) sia al numeratore sia al denominatore e 
semplifichiamo;
\item dato che \(f(M)\) è un finito e non dipende dal valore di 
\(M\), applichiamo la definizione di limite finito;
\item la ps del quoziente tra un finito e un non infinitesimo è 
uguale al quoziente delle ps; 
\item la ps della somma tra un non infinitesimo e un infinitesimo è il non 
infinitesimo. E dato che il valore ottenuto è indipendente dal valore di 
\(M\) questo è effettivamente il limite cercato.
\end{enumerate}

\begin{comment}
Direi che sostanzialmente va bene. Ti propongo solo un paio di suggerimenti 
per migliorare ulteriormente.
  Nella penultima riga a pagina 3 dici "qualunque sia il valore di M". Questa 
espressione, che deve esserci, fa perdere la convenzione su M, sicché sarebbe 
meglio dire "qualunque sia il valore infinito positivo di M".
  Alla prima riga di pagina 4, alla fine della riga aggiungerei "positivo".
  Nello sviluppo del terzo esempio, sempre a pagina 4, al punto 2 dici che si 
sostituiscono espressioni con una indistinguibile, ma direi che almeno 
sarebbe bene precisare quali sono le proposizioni sostituite e quali quelle 
rispettivamente indistinguibili da esse.
  Sempre nello sviluppo del terzo esempio, alla fine della penultima frase, 
dici "può aiutare a capire meglio la differenza tra "grandissimo" e 
"infinito" e allena all'uso del concetto di "ordine" di infinitesimo e di 
infinito. Non riesco a capire la differenza che intendi far rilevare tra 
"grandissimo" e "infinito" né come ciò incida sul concetto di ordine tra 
infinitesimi e infiniti dal momento che questo concetto non viene neppure 
presentato.
  Ancora, sempre nello sviluppo del terzo esempio, alla prima riga 
dell'ultima frase dici "ci assicura"; io piuttosto direi "coglie".
\end{comment}


\vspace{1em}
\textbf{Terzo esempio, secondo modo}\\
Il metodo precedente ricalca il metodo proposto nei normali corsi di analisi, 
ma, se studiando i numeri iperreali abbiamo introdotto la relazione 
di indistinguibilità (simbolo: \(\sim\)) e gli ordini di infinitesimo e di 
infinito, possiamo seguire una strada più semplice.

% La \emph{relazione di indistinguibilità} 
% (\(\sim\)), che non abbiamo trattato nel corso perché non è strettamente 
% necessaria, permette di semplificare i calcoli. 

La \emph{relazione di indistinguibilità} si può affrontare quando 
si studia il confronto tra numeri iperreali.

L'indistinguibilità, oltre a semplificare alcuni calcoli, 
può aiutare a capire meglio la differenza tra ``grandissimo'' e 
``infinito'' e allena all'uso del concetto di ``ordine'' di infinitesimo 
e di infinito.

In questo caso l'indistinguibilità coglie l'idea intuitiva che 
aggiungere 7 ad una grandezza infinita non cambia la quantità in modo 
percettibile: \(3M^2-3M+7 \sim 3M^2-3M\) e 
che se tolgo \(3\) \emph{infiniti} da 
\(3\) \emph{infiniti di questi infiniti} 
non modifico la situazione in modo percettibile:
\(3M^2-3M \sim 3M^2\).
In modo analogo: \(5M^2-6 \sim 5M^2\).

Studia il comportamento della funzione 
\(f(x) = \dfrac{3x^2-3x+7}{5x^2-6}\)
per \(x \to +\infty\). 
\[f(M) \stackrel{1}{=} \frac{3M^2-3M+7}{5M^2-6}\stackrel{2}{\sim} 
  \frac{3\cancel{M^2}}{5\cancel{M^2}} = \frac{3}{5}\]
Dato che il valore ottenuto è finito 
(il quoziente di due finiti non infinitesimi), otterremo un limite finito:
\[\lim_{x \to +\infty} \frac{3x^2-3x+7}{5x^2-6} \stackrel{3}{=} 
\pst{f(M)} = \pst{\frac{3}{5}} \stackrel{4}{=} \frac{3}{5}\]

\begin{enumerate} [nosep]
\item Calcoliamo \(f(M)\), un generico iperreale infinito;
\item sostituiamo l'espressione ottenuta con una espressione 
  \emph{indistinguibile} e eseguiamo i calcoli; 
\item dato che il valore ottenuto è un finito, 
  il limite è la sua la parte standard;
\item la parte standard di un razionale è il razionale stesso.
\end{enumerate}

\vspace{1em}
\subsection*{Asintoti verticali}

In Analisi Standard \(x = c\) è un asintoto verticale se il limite destro o 
il limite sinistro per \(x\) che tende a \(c\) è infinito.

La stessa definizione vale anche in Analisi Non Standard, ma qui possiamo 
anche evitare di passare attraverso i limiti. 
Potremo dire che: \(x = c\) è un asintoto verticale se:
\begin{itemize} [nosep]
\item per ogni infinitesimo \(\epsilon > 0\) \(f(c+\epsilon)\) 
è un infinito positivo (negativo) o 
\item per ogni infinitesimo \(\epsilon < 0\) \(f(c+\epsilon)\) 
è un infinito positivo (negativo).
\end{itemize}

\pagebreak %-----------------------------------------

\subsection*{Tornando allo spunto iniziale}

\subsubsection{Asintoti verticali}
La funzione \(f(x) = \dfrac{1}{x}\) ha due rami infiniti che si avvicinano 
alla retta \(x = 0\) che è quindi un asintoto verticale doppio. 
Infatti: \(f(x + \epsilon)=\dfrac{1}{0 + \epsilon}\) è un infinito:
\begin{itemize} [nosep]
\item negativo se l'infinitesimo \(\epsilon < 0\),
\item positivo se l'infinitesimo \(\epsilon > 0\).
\end{itemize}
Anche la funzione \(f(x) = \dfrac{1}{x^2}\) ha \(x = 0\) come asintoto 
verticale doppio.
Infatti: \(f(x + \epsilon)=\dfrac{1}{\epsilon^2}\) è un infinito:
\begin{itemize} [nosep]
\item positivo se l'infinitesimo \(\epsilon < 0\),
\item positivo se l'infinitesimo \(\epsilon > 0\).
\end{itemize}

\subsubsection{Limiti}
Se invece vogliamo studiare il comportamento delle due funzioni nei pressi di 
zero, calcoliamo i limiti: \qquad 
\(\displaystyle \lim_{x \to 0} \frac{1}{x}\) 
\quad e \quad \(\displaystyle \lim_{x \to 0} \frac{1}{x^2}\). 

\vspace{.5em}
Nel caso di \(f(x) = \dfrac{1}{x}\) per prima cosa calcoliamo: 
\(f(\epsilon) = \dfrac{1}{\epsilon}\) che è un valore infinito. \\
Dato che il segno di questo infinito dipende dal valore di \(\epsilon\), 
non esiste il limite. \\
Osservando però che il segno dell'infinito ottenuto è uguale al segno di 
\(\epsilon\) possiamo concludere che:
\[\lim_{x \to 0^-} \frac{1}{x} = -\infty \qquad 
\lim_{x \to 0^+} \frac{1}{x} = +\infty\]

Nel caso di \(f(x) = \dfrac{1}{x^2}\) per prima cosa calcoliamo: 
\(f(\epsilon) = \dfrac{1}{\epsilon^2}\). \\
Dato che \(f(\epsilon)\) è un infinito positivo per 
qualunque valore di \(\epsilon\):
\[\lim_{x \to 0} \frac{1}{x^2} = +\infty\]

\end{document}
