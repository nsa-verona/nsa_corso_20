%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        Corso Analisi Non Standard - Verona 2020
%
%                      Ruggero Ferro
%
%             Limiti in analisi non standard
%
%------------------------------
% Compilazione:
% pdflatex --shell-escape 01_nsa_limiti.tex
%------------------------------
%
% This work may be distributed and/or modified under the
% conditions of the Creative Commons BY-SA
%
% Authors: Ruggero Ferro
%
% The Current Maintainer of this work is 
% Daniele Zambelli - daniele.zambelli@gmail.com
%
% Copyright 2020-24 Bruno Stecca
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%========================
% Definizione delle directory
%========================
\newcommand{\depdir}{../../}
\newcommand{\lbr}{\depdir lbr/}

%========================
% Tipo di documento
%========================
\documentclass[a4paper, 12pt]{report}
\usepackage[margin=20mm]{geometry}

%========================
% Lettura preambolo e definizioni
%========================
\input{\lbr preambolo_guide}
\input{\lbr definizioni_guide}
\graphicspath{ {./img/} }

%========================
% Grafici 
%========================
\newcommand{\microscopioxp}{
    \microscopio{(2, 0)}{3}{20}{230}{2}{(7.7, 4.3)}
                {\scriptsize \(\times \infty\)}
    \draw (4.2, 2) --(8.05, 2);
    \draw [dashed] 
          (\xc, 4.3) -- (\xc, 2) node [left, rotate=90]
                                      {\scriptsize \(c\)}
          (\xcmeps, 4.3) -- (\xcmeps, 2) node [left, rotate= 90]  
                                      {\scriptsize \(c-\epsilon\)}
          (\xcpeps, 4.3) -- (\xcpeps, 2) node [left, rotate= 90] 
                                      {\scriptsize \(c+\epsilon\)};
}

\newcommand{\microscopioyp}{
  \def \ay{.3}
    \microscopio{(0, 4)}{1}{120}{250}{2}{(1.8, 8.5)}
                {\scriptsize \(\times \infty\)}
    \draw (\ay, 4.75) --(\ay, 8.75);
    \draw [dashed] 
          (2, \yl) -- (\ay, \yl) node [left=-.15] {\scriptsize \(L\)}
          (1.5, \ylmeps) -- (\ay, \ylmeps) node [left=-.15] 
          {\scriptsize \(L-\delta\)}
          (1.9, \ylpeps) -- (\ay, \ylpeps) node [left=-.15] 
          {\scriptsize \(L+\delta\)};
}

\newcommand{\microscopiop}{
    \microscopio{(2, 4)}{3}{20}{230}{2}{(7.5, 8.5)}
                {\scriptsize \(\times \infty\)}
    \tkzInit[xmin=0,xmax=+8.2,ymin=0,ymax=+8.3]
    \tkzFct[ultra thick, color=red!50!black, domain=4.95:7.17] {1.5*x-2.5} 
    \filldraw [color=black, fill=white] (\xc, \yl) circle (1.5pt);
%     \draw (6.2, 5.75) node [below, xshift=-1mm] {\scriptsize \(\epsilon\)};
    \draw [dashed] 
          (4.1, \yl) -- (\xc, \yl) -- (\xc, 4.5)
          (4.5, \ylmeps) -- (\xcmeps, \ylmeps) -- (\xcmeps, 4.8)
          (4.5, \ylpeps) -- (\xcpeps, \ylpeps) -- (\xcpeps, 4.8);
}

\newcommand{\limite}{% 
  % Tangente ad una parabola nel punto (5; 4).
  \def \xc{6}
  \def \xcmeps{5.2}
  \def \xcpeps{6.8}
  \def \yl{6.5}
  \def \ylmeps{5.3}
  \def \ylpeps{7.7}
  \disegno[4]{
    \rcom{0}{+8}{0}{9}{gray!50, very thin, step=1}
    \tkzInit[xmin=-0.3,xmax=+8.3,ymin=-0.3,ymax=+9.3]
    \tkzFct[ultra thick, color=red!50!black,
            domain=-2.3:+8.3]{0.5*x*x-1./2*x+3} 
    \filldraw [color=black, fill=white] (2, 4) circle (1.5pt);
    \draw [dashed] (2, 4) -- (2, 0) 
          node [black, below] 
              {\scriptsize \(c \approx c+\epsilon \approx c-\epsilon\)}
          (2, 4) -- (0, 4) 
          node [black, left] 
              {\scriptsize \(L \approx f(c+\epsilon) \approx f(c-\epsilon)\)};
    \microscopioxp    % Microscopio su x_P
    \microscopioyp    % Microscopio su y_P
    \microscopiop     % Microscopio su P
    }
}

\begin{document}
\begin{center}
\Large{
\textbf{Nozione di limite}}

\emph{Ruggero Ferro}
\end{center}

\subsection*{Limite nell'analisi non standard}

Nel definire la derivata di una funzione si è adoperata un'operazione che è 
presente anche nelle definizioni di altre nozioni importanti, la continuità di 
una funzione ad esempio. 
Di fatto si è conside­rata la parte standard di una quantità (nel caso 
particolare visto, il rapporto incrementale), quantità che 
dipende da un'altra (nello stesso caso visto, l'incremento della variabile 
indipendente), quando questa è infinitamente vicina ma non uguale a un certo 
valore (lo 0, nel caso esaminato). 
In genera­le, data una funzione \(f\) e un valore reale \(c\), questo passaggio 
associa loro un numero reale \(L\) che è la parte standard dell'immagine 
attraverso la funzione \(f\) di ogni iperreale \(x\) infinitamente vicino a 
\(c\), ma diverso da \(c\), se questa c'è ed è sempre la stessa; in simboli: 
\(L = \pst{f(x)}\) 
se questa c'è ed è sempre la stessa per ogni \(x\) infinitamente vicino 
a \(c\) e diverso da \(c\).

L'operazione evidenziata fornisce la nozione non standard di limite della 
funzione \(f\) quando la sua variabile indipendente tende a \(c\) (sarebbe 
meglio dire quando la variabile indipendente assume valori infinitamente
vicini a \(c\) ma diversi da \(c\), ma non è pericoloso seguire il modo di 
esprimersi consueto). 
Per quest'operazione si userà la notazione 
\(\displaystyle \lim_{x \to c}f(x)\).

Detto altrimenti, poiché ogni iperreale infinitamente vicino a \(c\) è del 
tipo \(c\) più un infinitesimo, il limite della funzione \(f\) per \(x\) 
tendente a \(c\) è \(\pst{f(c + \epsilon)}\), 
cioè la parte standard della funzione calcolata in \(c + \epsilon\), se 
questa c'è ed è sempre la stessa per ogni infinitesimo non nullo 
\(\epsilon\). 

Si noti la totale differenza di questa nozione da quella classica di limite. 
Ciononostante, per loro viene proposta la stessa notazione per motivi che si 
vedranno dopo aver richiamato esplicitamente la nozione classica.

\noindent\begin{minipage}{.39\textwidth}
La figura seguente illustra l'operazione definita, dove all'interno dei 
tondi è rappresentato un in­grandimento della porzione di piano infinitamente 
vicina al punto cui il tondo è associato, ingrandi­mento alla scala che 
permette di vedere il generico infinitesimo \(\epsilon\) considerato.
\end{minipage}
\hfill
\begin{minipage}{.59\textwidth}
% \begin{center}
\scalebox{1.3}{\limite}
% \end{center}
\end{minipage}

Il cerchietto vuoto indica che non si considera il comportamento della 
funzione nel punto di ascissa \(c\).

L'operazione di limite sintetizza il comportamento della funzione \(f\) 
nell'infinitamente vicino a \(c\) e diverso da \(c\), comportamento che 
consiste nell'avere tutti i valori della funzione, corrispondenti ai punti 
infinitamente vicini a \(c\) e diversi da \(c\), infinitamente vicini a un 
unico numero reale \(L\).

La definizione data permette di calcolare il limite, se c'è: basta considerare 
il valore \(f(c + \epsilon)\), con \(\epsilon\) un qualsiasi infinitesimo non 
nullo, ed esprimerlo in modo da isolare le parti trascurabili (infinitesi­me) e 
trascurarle nel passaggio alla parte standard.

È naturale pensare una funzione come continua in un intervallo se il suo 
grafico può essere disegnato con un tratto continuo senza interruzioni. 
Questo modo di vedere può essere colto adeguatamente dalla richiesta che a 
variazioni infinitesime della variabile indipendente corrispondono variazioni 
infinitesime dei valori della funzione. 
Così si dirà che una funzione \(f\) è continua in un 
intervallo chiuso \(\intervcc{h}{k}\) (di fatto uniformemente continua) se per 
ogni scelta di due punti infinitamente vicini \(x_1\) e \(x_2\) in 
\(\intervcc{h}{k}\) anche \(f(x_1)\) e \(f(x_2)\) sono 
infinitamente vicini.

Si può anche considerare la nozione di continuità di una funzione in un punto 
reale \(c\) interno al suo dominio. 
La precedente definizione si riduce a richiedere che \(f(x)\) sia 
infinitamente vicino a \(f(c)\) per ogni punto \(x\) infinitamente vicino a 
\(c\). 

La condizione enunciata può essere espressa anche mediante l'operazione di 
passaggio al limite. 
Con la notazione introdotta si può dire che una funzione \(f\) 
è continua in \(c\) interno al suo dominio se 
\(\displaystyle f(c) = \lim_{x \to c}f(x)\), cioè 
\(f(c) = \pst{f(c + \epsilon)}\) per ogni infinitesimo \(\epsilon\). 

Volendo poi parlare di continuità puntuale 
in un intervallo chiuso dovremo anche precisare cosa deve succedere agli 
estremi dell'intervallo \(\intervcc{a}{b}\): oltre alla continuità puntuale 
nei punti interni all'inter­vallo, si richiederà che, per ogni infinitesimo 
positivo \(\epsilon\), siano 
\(\pst{f(a + \epsilon)} \approx f(a) \texte 
\pst{f(b - \epsilon)} \approx f(b)\). 
Questa nozione di continuità di una funzione in un intervallo chiuso è molto 
importante perché per le funzioni di questo tipo si otterranno particolari 
teoremi fondamentali le cui dimostrazioni non standard sono molto più semplici 
delle corrispondenti dimostrazioni standard.

\subsection*{Confronto tra nozioni di limite}

L'introduzione e l'uso degli infinitesimi si erano dimostrati molto efficaci 
per lo sviluppo delle basi dell'analisi matematica, ma una volta che questi 
furono rifiutati per preconcetto, si pose il problema di recuperare i 
risultati ottenuti mediante loro senza poter disporre del loro intervento 
esplicito. 

La soluzione trovata nell'Ottocento fu un'opportuna diversa nozione di limite, 
mediante la quale si poterono definire continuità, derivate, integrali e 
quant'altro. 
Tale nozione di limite si basa sulla nozione di ``piccolo''.

La nozione di piccolo non può essere assoluta, ma è solo relativa: una 
quantità piccola su scala astronomica non è detto che sia piccola su scala 
nucleare, si può parlare solo di più piccolo di una quantità fissata.
La nozione di piccolo non si preserva operando su di essa: una somma di ``più 
piccoli di una certa quantità'' non è detto che sia ``più piccola di quella 
quantità''.

La nuova definizione di limite, cui si era giunti con la nozione di ``più 
piccolo di'' e senza fare riferimento agli infinitesimi, cerca di cogliere il 
comportamento di una funzione nelle piccole distanze da un punto \(c\). 
Essa dice che \(L\) è il limite di \(f(x)\) per \(x\) tendente a \(c\) se
\[\forall \epsilon > 0 \quad \exists \delta > 0 \quad \forall x
\tonda{\tonda{x \neq c \sand \abs{x-c}< \delta} \srarrow 
\abs{f(x)-L}< \epsilon}\]
con la presenza di piccoli relativi come ``più piccolo di \(\delta\)'' e 
``più piccolo di \(\epsilon\)''.

Quant'è difficile capire e spiegare perché questa debba essere la nozione di 
limite: è un gioco competitivo a due in cui il secondo giocatore ha una 
strategia vincente se il numero che si ritiene sia il limite lo è davvero. 

Cioè, di fronte alla sfida lanciata dal primo giocatore di avvicinare il 
risultato più di una qualsiasi quantità reale da lui prefissata, il secondo 
giocatore deve trovare dei margini d'intervento tali che ogni azione scelta 
entro quei margini permetta di superare la sfida.

Mentre la nozione non standard di limite usa una sola quantificazione del tipo 
``per ogni'', ora si affronta un problema del tipo ``per ogni - esiste - per 
ogni''. 

Ma com'è complicato cogliere l'operare delle tre quantificazioni 
alternate esattamente in quell'ordine. 
Esse indicano che la risoluzione del problema sta nel far corrispondere a una 
qualsiasi precisione assegnata (per ogni distanza massima richiesta dal 
limite) un opportuno livello di precisione 
(esiste un intorno di \(c\) privato di \(c\)) in modo che ogni azione fatta 
entro la precisione che è stata determinata (per ogni valore appartenente 
al­l'intorno di \(c\) e diverso da \(c\)) porta a superare la prova.


In più, nonostante la direzione dell'implicazione \\
\(\tonda{x \neq c \sand \abs{x-c}< \delta} \srarrow \abs{f(x)-L}< \epsilon\)
vada dal \(\delta\) che si deve trovare all'\(\epsilon\) che è dato, si deve 
partire dalla limitazione \(\epsilon\) della precisione richiesta di distanza 
da \(L\), per arrivare a determinare la limitazione \(\delta\) della 
precisione concessa ai dati: \(\forall \epsilon > 0\) precede 
\(\exists \delta > 0\). 

Invece, con i metodi non standard, si parte dai dati, pur approssimati ma 
a meno di quantità assolutamente trascurabili (\(x\) diverso da \(c\) e 
infinitamente vicino a \(c\)), per giungere al risultato \(f(x)\), ancora 
approssimato a \(L\), ma a meno di quantità assolutamente trascurabili.

Peggio, questa definizione standard permette solo di verificare che un dato 
numero \(L\) è il limite, ma non dice come trovarlo. 
Per fare ciò, bisogna utilizzare i teoremi sui limiti (il limite della somma è 
la somma dei limiti, ecc.) le cui dimostrazioni richiedono di avere 
acquisito bene l'orribile definizione classica di limite.

Le difficoltà notate giustificano l'affermazione iniziale d'inutile 
complicazione dell'approccio classico all'analisi matematica.

La nuova definizione di limite senza gli infinitesimi coglie tuttavia 
correttamente la nozione di limite con gli infinitesimi: di fatto si può 
dimostrare abbastanza agevolmente (una volta superata la difficoltà di 
digerire la nuova definizione) che un certo numero è il limite nella nozione 
non standard se e solo se lo è nella nozione, cosiddetta \(\epsilon \delta\), 
ora definita.

Di fatto è opportuno introdurre, a tempo debito, la nozione 
\(\epsilon \delta\) di limite anche in una trattazione non standard per fare 
considerazioni sul comportamento nel relativamente vicino (ad esempio per 
completare un grafico). 
Tuttavia, la nozione \(\epsilon \delta\) può essere presto abbandonata dopo 
aver mostrato che il comportamento nel relativamente vicino è lo stesso di 
quello che si manifesta nell'infinitamente vicino.

A scuola normalmente non si dimostrano i teoremi che forniscono gli strumenti 
per giungere alla individuazione del valore del limite: essi sono sostituiti 
da regole, e la matematica è umiliata e perde il suo significato.

Vorrei proporre un paio di slogan che colgano le differenze tra le due nozioni 
di limite presentate.

La nozione \emph{non standard} corrisponde a:
\begin{center}\emph{se m'impegno moltissimo, faccio benissimo},\end{center}
con i superlativi assoluti.

La nozione \emph{classica} corrisponde a:
\begin{center}\emph{so impegnarmi abbastanza da far meglio di quanto 
richiesto, \\
qualunque sia il traguardo voluto},\end{center}
con i superlativi relativi.

Già dagli slogan si può notare la differente difficoltà tra le due nozioni.

C'è poi lo slogan che coglie 
la \emph{presentazione dei limiti in molti testi scolastici}: 
\begin{center}\emph{più m'impegno, meglio faccio}.\end{center} 
Esso riduce al relativo l'affermazione che nell'analisi non standard usa i 
superlativi assoluti. 
Però l'atteggiamento così individuato è un grave travisamento della nozione di 
limite che non può essere colta in questo modo. 
Infatti, nessuno garantisce che pur facendo sempre meglio non si rimanga 
comunque lontani dal traguardo. 
L'errata nozione colta da questo slogan ostacola gravemente la comprensione 
della nozione di limite.

\end{document}
