# README # 

### A cosa è dedicato questo repository? ###

Questo repository è pensato per contenere i materiali del corso per i docenti
organizzato dal gruppo NSA di Verona.

Il corso vuole proporre un'introduzione all'Analisi Non Standard e
al suo uso nella didattica della matematica.

### Come posso usarlo? ###

Il repository contiene tutti i sorgenti LaTeX delle presentazioni.

Nella sezione Download verranno rese disponibili:

* le presentazioni in formato pdf;
* i video delle lezioni;
* i video degli incontri.

### Come contribuire? ###

Attraverso i Ticket (barra a sinistra) si possono segnalare:

* errori;
* Osservazioni;
* proposte
* ...

### Come contattarci? ###

* Si può scrivere all'amministratore del sito: daniele.zambelli@gmail.com
* Ci si può iscrivere alla Mailing List: nsa_verona@framalistes.org
